const images = [{
        "image": "https://wger.de/media/exercise-images/4/Crunches-1.png",
        "exercise": 4
    },
    {
        "image": "https://wger.de/media/exercise-images/4/Crunches-2.png",
        "exercise": 4
    },
    {
        "image": "https://wger.de/media/exercise-images/91/Crunches-1.png",
        "exercise": 91
    },
    {
        "image": "https://wger.de/media/exercise-images/91/Crunches-2.png",
        "exercise": 91
    },
    {
        "image": "https://wger.de/media/exercise-images/32/Decline-crunch-1.png",
        "exercise": 32
    },
    {
        "image": "https://wger.de/media/exercise-images/32/Decline-crunch-2.png",
        "exercise": 32
    },
    {
        "image": "https://wger.de/media/exercise-images/93/Decline-crunch-1.png",
        "exercise": 93
    },
    {
        "image": "https://wger.de/media/exercise-images/93/Decline-crunch-2.png",
        "exercise": 93
    },
    {
        "image": "https://wger.de/media/exercise-images/60/Hyperextensions-1.png",
        "exercise": 60
    },
    {
        "image": "https://wger.de/media/exercise-images/60/Hyperextensions-2.png",
        "exercise": 60
    },
    {
        "image": "https://wger.de/media/exercise-images/128/Hyperextensions-1.png",
        "exercise": 128
    },
    {
        "image": "https://wger.de/media/exercise-images/128/Hyperextensions-2.png",
        "exercise": 128
    },
    {
        "image": "https://wger.de/media/exercise-images/38/Narrow-grip-bench-press-1.png",
        "exercise": 38
    },
    {
        "image": "https://wger.de/media/exercise-images/38/Narrow-grip-bench-press-2.png",
        "exercise": 38
    },
    {
        "image": "https://wger.de/media/exercise-images/88/Narrow-grip-bench-press-1.png",
        "exercise": 88
    },
    {
        "image": "https://wger.de/media/exercise-images/88/Narrow-grip-bench-press-2.png",
        "exercise": 88
    },
    {
        "image": "https://wger.de/media/exercise-images/3/Standing-biceps-curl-1.png",
        "exercise": 3
    },
    {
        "image": "https://wger.de/media/exercise-images/3/Standing-biceps-curl-2.png",
        "exercise": 3
    },
    {
        "image": "https://wger.de/media/exercise-images/129/Standing-biceps-curl-1.png",
        "exercise": 129
    },
    {
        "image": "https://wger.de/media/exercise-images/129/Standing-biceps-curl-2.png",
        "exercise": 129
    },
    {
        "image": "https://wger.de/media/exercise-images/26/Biceps-curl-1.png",
        "exercise": 26
    },
    {
        "image": "https://wger.de/media/exercise-images/26/Biceps-curl-2.png",
        "exercise": 26
    },
    {
        "image": "https://wger.de/media/exercise-images/81/Biceps-curl-1.png",
        "exercise": 81
    },
    {
        "image": "https://wger.de/media/exercise-images/81/Biceps-curl-2.png",
        "exercise": 81
    },
    {
        "image": "https://wger.de/media/exercise-images/24/Bicep-curls-1.png",
        "exercise": 24
    },
    {
        "image": "https://wger.de/media/exercise-images/24/Bicep-curls-2.png",
        "exercise": 24
    },
    {
        "image": "https://wger.de/media/exercise-images/74/Bicep-curls-1.png",
        "exercise": 74
    },
    {
        "image": "https://wger.de/media/exercise-images/74/Bicep-curls-2.png",
        "exercise": 74
    },
    {
        "image": "https://wger.de/media/exercise-images/29/Tricep-dips-2-1.png",
        "exercise": 29
    },
    {
        "image": "https://wger.de/media/exercise-images/29/Tricep-dips-2-2.png",
        "exercise": 29
    },
    {
        "image": "https://wger.de/media/exercise-images/82/Tricep-dips-2-1.png",
        "exercise": 82
    },
    {
        "image": "https://wger.de/media/exercise-images/82/Tricep-dips-2-2.png",
        "exercise": 82
    },
    {
        "image": "https://wger.de/media/exercise-images/68/Bench-dips-1.png",
        "exercise": 68
    },
    {
        "image": "https://wger.de/media/exercise-images/68/Bench-dips-2.png",
        "exercise": 68
    },
    {
        "image": "https://wger.de/media/exercise-images/83/Bench-dips-1.png",
        "exercise": 83
    },
    {
        "image": "https://wger.de/media/exercise-images/83/Bench-dips-2.png",
        "exercise": 83
    },
    {
        "image": "https://wger.de/media/exercise-images/8/Dumbbell-shrugs-2.png",
        "exercise": 8
    },
    {
        "image": "https://wger.de/media/exercise-images/8/Dumbbell-shrugs-1.png",
        "exercise": 8
    },
    {
        "image": "https://wger.de/media/exercise-images/151/Dumbbell-shrugs-2.png",
        "exercise": 151
    },
    {
        "image": "https://wger.de/media/exercise-images/151/Dumbbell-shrugs-1.png",
        "exercise": 151
    },
    {
        "image": "https://wger.de/media/exercise-images/137/Barbell-shrugs-1.png",
        "exercise": 137
    },
    {
        "image": "https://wger.de/media/exercise-images/137/Barbell-shrugs-2.png",
        "exercise": 137
    },
    {
        "image": "https://wger.de/media/exercise-images/150/Barbell-shrugs-1.png",
        "exercise": 150
    },
    {
        "image": "https://wger.de/media/exercise-images/150/Barbell-shrugs-2.png",
        "exercise": 150
    },
    {
        "image": "https://wger.de/media/exercise-images/46/Bicep-hammer-curl-1.png",
        "exercise": 46
    },
    {
        "image": "https://wger.de/media/exercise-images/46/Bicep-hammer-curl-2.png",
        "exercise": 46
    },
    {
        "image": "https://wger.de/media/exercise-images/86/Bicep-hammer-curl-1.png",
        "exercise": 86
    },
    {
        "image": "https://wger.de/media/exercise-images/86/Bicep-hammer-curl-2.png",
        "exercise": 86
    },
    {
        "image": "https://wger.de/media/exercise-images/134/Hammer-curls-with-rope-1.png",
        "exercise": 134
    },
    {
        "image": "https://wger.de/media/exercise-images/134/Hammer-curls-with-rope-2.png",
        "exercise": 134
    },
    {
        "image": "https://wger.de/media/exercise-images/138/Hammer-curls-with-rope-1.png",
        "exercise": 138
    },
    {
        "image": "https://wger.de/media/exercise-images/138/Hammer-curls-with-rope-2.png",
        "exercise": 138
    },
    {
        "image": "https://wger.de/media/exercise-images/172/Push-ups-1.png",
        "exercise": 172
    },
    {
        "image": "https://wger.de/media/exercise-images/195/Push-ups-1.png",
        "exercise": 195
    },
    {
        "image": "https://wger.de/media/exercise-images/172/Push-ups-2.png",
        "exercise": 172
    },
    {
        "image": "https://wger.de/media/exercise-images/195/Push-ups-2.png",
        "exercise": 195
    },
    {
        "image": "https://wger.de/media/exercise-images/25/Lying-close-grip-triceps-press-to-chin-1.png",
        "exercise": 25
    },
    {
        "image": "https://wger.de/media/exercise-images/25/Lying-close-grip-triceps-press-to-chin-2.png",
        "exercise": 25
    },
    {
        "image": "https://wger.de/media/exercise-images/84/Lying-close-grip-triceps-press-to-chin-1.png",
        "exercise": 84
    },
    {
        "image": "https://wger.de/media/exercise-images/84/Lying-close-grip-triceps-press-to-chin-2.png",
        "exercise": 84
    },
    {
        "image": "https://wger.de/media/exercise-images/45/Seated-triceps-press-1.png",
        "exercise": 45
    },
    {
        "image": "https://wger.de/media/exercise-images/45/Seated-triceps-press-2.png",
        "exercise": 45
    },
    {
        "image": "https://wger.de/media/exercise-images/50/Good-mornings-2.png",
        "exercise": 50
    },
    {
        "image": "https://wger.de/media/exercise-images/50/Good-mornings-1.png",
        "exercise": 50
    },
    {
        "image": "https://wger.de/media/exercise-images/116/Good-mornings-2.png",
        "exercise": 116
    },
    {
        "image": "https://wger.de/media/exercise-images/116/Good-mornings-1.png",
        "exercise": 116
    },
    {
        "image": "https://wger.de/media/exercise-images/192/Bench-press-1.png",
        "exercise": 192
    },
    {
        "image": "https://wger.de/media/exercise-images/62/Barbell-upright-rows-2.png",
        "exercise": 62
    },
    {
        "image": "https://wger.de/media/exercise-images/192/Bench-press-2.png",
        "exercise": 192
    },
    {
        "image": "https://wger.de/media/exercise-images/62/Barbell-upright-rows-1.png",
        "exercise": 62
    },
    {
        "image": "https://wger.de/media/exercise-images/36/Chin-ups-2.png",
        "exercise": 36
    },
    {
        "image": "https://wger.de/media/exercise-images/181/Chin-ups-2.png",
        "exercise": 181
    },
    {
        "image": "https://wger.de/media/exercise-images/36/Chin-ups-1.png",
        "exercise": 36
    },
    {
        "image": "https://wger.de/media/exercise-images/181/Chin-ups-1.png",
        "exercise": 181
    },
    {
        "image": "https://wger.de/media/exercise-images/76/T-bar-row-1.png",
        "exercise": 76
    },
    {

        "image": "https://wger.de/media/exercise-images/106/T-bar-row-1.png",
        "exercise": 106
    },
    {
        "image": "https://wger.de/media/exercise-images/76/T-bar-row-2.png",
        "exercise": 76
    },
    {
        "image": "https://wger.de/media/exercise-images/106/T-bar-row-2.png",
        "exercise": 106
    },
    {
        "image": "https://wger.de/media/exercise-images/59/Barbell-rear-delt-row-1.png",
        "exercise": 59
    },
    {
        "image": "https://wger.de/media/exercise-images/59/Barbell-rear-delt-row-2.png",
        "exercise": 59
    },
    {
        "image": "https://wger.de/media/exercise-images/109/Barbell-rear-delt-row-1.png",
        "exercise": 109
    },
    {
        "image": "https://wger.de/media/exercise-images/70/Reverse-grip-bent-over-rows-1.png",
        "exercise": 70
    },
    {

        "image": "https://wger.de/media/exercise-images/70/Reverse-grip-bent-over-rows-2.png",
        "exercise": 70
    },
    {
        "image": "https://wger.de/media/exercise-images/109/Barbell-rear-delt-row-2.png",
        "exercise": 109
    },
    {
        "image": "https://wger.de/media/exercise-images/110/Reverse-grip-bent-over-rows-1.png",
        "exercise": 110
    },
    {
        "image": "https://wger.de/media/exercise-images/110/Reverse-grip-bent-over-rows-2.png",
        "exercise": 110
    },
    {
        "image": "https://wger.de/media/exercise-images/193/Preacher-curl-3-1.png",
        "exercise": 193
    },
    {
        "image": "https://wger.de/media/exercise-images/193/Preacher-curl-3-2.png",
        "exercise": 193
    },
    {
        "image": "https://wger.de/media/exercise-images/11/Rear-deltoid-row-2.png",
        "exercise": 11
    },
    {
        "image": "https://wger.de/media/exercise-images/11/Rear-deltoid-row-1.png",
        "exercise": 11
    },
    {
        "image": "https://wger.de/media/exercise-images/15/Bench-press-1.png",
        "exercise": 15
    },
    {
        "image": "https://wger.de/media/exercise-images/15/Bench-press-2.png",
        "exercise": 15
    },
    {
        "image": "https://wger.de/media/exercise-images/77/One-arm-bench-press-1.png",
        "exercise": 77
    },
    {
        "image": "https://wger.de/media/exercise-images/77/One-arm-bench-press-2.png",
        "exercise": 77
    },
    {
        "image": "https://wger.de/media/exercise-images/18/Dumbbell-flys-1.png",
        "exercise": 18
    },
    {
        "image": "https://wger.de/media/exercise-images/18/Dumbbell-flys-2.png",
        "exercise": 18
    },
    {
        "image": "https://wger.de/media/exercise-images/73/Dumbbell-decline-flys-2.png",
        "exercise": 73
    },
    {
        "image": "https://wger.de/media/exercise-images/16/Incline-press-1.png",
        "exercise": 16
    },
    {
        "image": "https://wger.de/media/exercise-images/16/Incline-press-2.png",
        "exercise": 16
    },
    {
        "image": "https://wger.de/media/exercise-images/61/Close-grip-bench-press-1.png",
        "exercise": 61
    },
    {
        "image": "https://wger.de/media/exercise-images/61/Close-grip-bench-press-2.png",
        "exercise": 61
    },
    {
        "image": "https://wger.de/media/exercise-images/30/Butterfly-machine-2.png",
        "exercise": 30
    },
    {
        "image": "https://wger.de/media/exercise-images/30/Butterfly-machine-1.png",
        "exercise": 30
    },
    {
        "image": "https://wger.de/media/exercise-images/98/Butterfly-machine-2.png",
        "exercise": 98
    },
    {
        "image": "https://wger.de/media/exercise-images/98/Butterfly-machine-1.png",
        "exercise": 98
    },
    {
        "image": "https://wger.de/media/exercise-images/97/Dumbbell-bench-press-1.png",
        "exercise": 97
    },
    {
        "image": "https://wger.de/media/exercise-images/97/Dumbbell-bench-press-2.png",
        "exercise": 97
    },
    {
        "image": "https://wger.de/media/exercise-images/100/Decline-bench-press-1.png",
        "exercise": 100
    },
    {
        "image": "https://wger.de/media/exercise-images/100/Decline-bench-press-2.png",
        "exercise": 100
    },
    {
        "image": "https://wger.de/media/exercise-images/163/Incline-bench-press-1.png",
        "exercise": 163
    },
    {
        "image": "https://wger.de/media/exercise-images/163/Incline-bench-press-2.png",
        "exercise": 163
    },
    {
        "image": "https://wger.de/media/exercise-images/122/Incline-cable-flyes-1.png",
        "exercise": 122
    },
    {
        "image": "https://wger.de/media/exercise-images/122/Incline-cable-flyes-2.png",
        "exercise": 122
    },
    {
        "image": "https://wger.de/media/exercise-images/113/Walking-lunges-1.png",
        "exercise": 113
    },
    {
        "image": "https://wger.de/media/exercise-images/130/Narrow-stance-hack-squats-1-1024x721.png",
        "exercise": 130
    },
    {
        "image": "https://wger.de/media/exercise-images/130/Narrow-stance-hack-squats-2-1024x721.png",
        "exercise": 130
    },
    {
        "image": "https://wger.de/media/exercise-images/71/Cable-crossover-2.png",
        "exercise": 71
    },
    {
        "image": "https://wger.de/media/exercise-images/71/Cable-crossover-1.png",
        "exercise": 71
    },
    {
        "image": "https://wger.de/media/exercise-images/41/Incline-bench-press-1.png",
        "exercise": 41
    },
    {
        "image": "https://wger.de/media/exercise-images/41/Incline-bench-press-2.png",
        "exercise": 41
    },
    {
        "image": "https://wger.de/media/exercise-images/9/Dead-lifts-2.png",
        "exercise": 9
    },
    {
        "image": "https://wger.de/media/exercise-images/9/Dead-lifts-1.png",
        "exercise": 9
    },
    {
        "image": "https://wger.de/media/exercise-images/17/Decline-bench-press-1.png",
        "exercise": 17
    },
    {
        "image": "https://wger.de/media/exercise-images/17/Decline-bench-press-2.png",
        "exercise": 17
    },
    {
        "image": "https://wger.de/media/exercise-images/56/Decline-crunch-1.png",
        "exercise": 56
    },
    {
        "image": "https://wger.de/media/exercise-images/56/Decline-crunch-2.png",
        "exercise": 56
    },
    {
        "image": "https://wger.de/media/exercise-images/34/Leg-raises-2.png",
        "exercise": 34
    },
    {
        "image": "https://wger.de/media/exercise-images/34/Leg-raises-1.png",
        "exercise": 34
    },
    {
        "image": "https://wger.de/media/exercise-images/125/Leg-raises-2.png",
        "exercise": 125
    },
    {
        "image": "https://wger.de/media/exercise-images/125/Leg-raises-1.png",
        "exercise": 125
    },
    {
        "image": "https://wger.de/media/exercise-images/37/Cable-seated-rows-2.png",
        "exercise": 37
    },
    {
        "image": "https://wger.de/media/exercise-images/37/Cable-seated-rows-1.png",
        "exercise": 37
    },
    {
        "image": "https://wger.de/media/exercise-images/143/Cable-seated-rows-2.png",
        "exercise": 143
    },
    {
        "image": "https://wger.de/media/exercise-images/143/Cable-seated-rows-1.png",
        "exercise": 143
    },
    {
        "image": "https://wger.de/media/exercise-images/161/Dead-lifts-2.png",
        "exercise": 161
    },
    {
        "image": "https://wger.de/media/exercise-images/161/Dead-lifts-1.png",
        "exercise": 161
    },
    {
        "image": "https://wger.de/media/exercise-images/176/Cross-body-crunch-1.png",
        "exercise": 176
    },
    {
        "image": "https://wger.de/media/exercise-images/176/Cross-body-crunch-2.png",
        "exercise": 176
    },
    {
        "image": "https://wger.de/media/exercise-images/191/Front-squat-1-857x1024.png",
        "exercise": 191
    },
    {
        "image": "https://wger.de/media/exercise-images/191/Front-squat-2-857x1024.png",
        "exercise": 191
    },
    {
        "image": "https://wger.de/media/exercise-images/111/Wide-stance-squat-2.gif",
        "exercise": 111
    },
    {
        "image": "https://wger.de/media/exercise-images/111/Wide-stance-squat-1.gif",
        "exercise": 111
    },
    {
        "image": "https://wger.de/media/exercise-images/7/Wide-stance-squat-2.gif",
        "exercise": 7
    },
    {
        "image": "https://wger.de/media/exercise-images/7/Wide-stance-squat-1.gif",
        "exercise": 7
    },
    {
        "image": "https://wger.de/media/exercise-images/5/Walking-lunges-1.png",
        "exercise": 5
    },
    {
        "image": "https://wger.de/media/exercise-images/5/Walking-lunges-2.png",
        "exercise": 5
    },
    {
        "image": "https://wger.de/media/exercise-images/5/Walking-lunges-3.png",
        "exercise": 5
    },
    {
        "image": "https://wger.de/media/exercise-images/5/Walking-lunges-4.png",
        "exercise": 5
    },
    {
        "image": "https://wger.de/media/exercise-images/55/Lunges-2-1.png",
        "exercise": 55
    },
    {
        "image": "https://wger.de/media/exercise-images/55/Lunges-2-2.png",
        "exercise": 55
    },
    {
        "image": "https://wger.de/media/exercise-images/6/Leg-press-1-1024x670.png",
        "exercise": 6
    },
    {
        "image": "https://wger.de/media/exercise-images/6/Leg-press-2-1024x670.png",
        "exercise": 6
    },
    {
        "image": "https://wger.de/media/exercise-images/54/Leg-press-1-1024x670.png",
        "exercise": 54
    },
    {
        "image": "https://wger.de/media/exercise-images/54/Leg-press-2-1024x670.png",
        "exercise": 54
    },
    {
        "image": "https://wger.de/media/exercise-images/39/Seated-leg-curl-1.png",
        "exercise": 39
    },
    {
        "image": "https://wger.de/media/exercise-images/39/Seated-leg-curl-2.png",
        "exercise": 39
    },
    {
        "image": "https://wger.de/media/exercise-images/177/Seated-leg-curl-1.png",
        "exercise": 177
    },
    {
        "image": "https://wger.de/media/exercise-images/177/Seated-leg-curl-2.png",
        "exercise": 177
    },
    {
        "image": "https://wger.de/media/exercise-images/72/Standing-leg-curl-2.png",
        "exercise": 72
    },
    {
        "image": "https://wger.de/media/exercise-images/72/Standing-leg-curl-1.png",
        "exercise": 72
    },
    {
        "image": "https://wger.de/media/exercise-images/119/seated-barbell-shoulder-press-large-1.png",
        "exercise": 119
    },
    {
        "image": "https://wger.de/media/exercise-images/123/dumbbell-shoulder-press-large-1.png",
        "exercise": 123
    },
    {
        "image": "https://wger.de/media/exercise-images/152/seated-shoulder-press-machine-large-1.png",
        "exercise": 152
    },
    {
        "image": "https://wger.de/media/exercise-images/148/lateral-dumbbell-raises-large-2.png",
        "exercise": 148
    },
    {
        "image": "https://wger.de/media/exercise-images/154/lying-leg-curl-machine-large-1.png",
        "exercise": 154
    },
    {
        "image": "https://wger.de/media/exercise-images/117/seated-leg-curl-large-1.png",
        "exercise": 117
    },
    {
        "image": "https://wger.de/media/exercise-images/118/standing-leg-curls-large-1.png",
        "exercise": 118
    },
    {
        "image": "https://wger.de/media/exercise-images/22/lying-leg-curl-machine-large-1.png",
        "exercise": 22
    },
    {
        "image": "https://wger.de/media/exercise-images/133/seated-leg-curl-large-1.png",
        "exercise": 133
    },
    {
        "image": "https://wger.de/media/exercise-images/231/Standing-biceps-curl-1.png",
        "exercise": 231
    },
    {
        "image": "https://wger.de/media/exercise-images/232/Triceps-kickback-1.png",
        "exercise": 232
    },
    {
        "image": "https://wger.de/media/exercise-images/228/Arnold-press-2.png",
        "exercise": 228
    },
    {
        "image": "https://wger.de/media/exercise-images/53/Shoulder-press-machine-2.png",
        "exercise": 53
    },
    {
        "image": "https://wger.de/media/exercise-images/241/Dumbbell-shoulder-press-2.png",
        "exercise": 241
    },
    {
        "image": "https://wger.de/media/exercise-images/266/Seated-military-shoulder-press-2.png",
        "exercise": 266
    },
    {
        "image": "https://wger.de/media/exercise-images/20/Dumbbell-lateral-raises-2.png",
        "exercise": 20
    },
    {
        "image": "https://wger.de/media/exercise-images/47/Lying-rear-lateral-raise-2.png",
        "exercise": 47
    },
    {
        "image": "https://wger.de/media/exercise-images/242/Incline-biceps-curl-1.png",
        "exercise": 242
    },
    {
        "image": "https://wger.de/media/exercise-images/13/Standing-calf-raises-1.png",
        "exercise": 13
    },
    {
        "image": "https://wger.de/media/exercise-images/244/Close-grip-front-lat-pull-down-2.png",
        "exercise": 244
    },
    {
        "image": "https://wger.de/media/exercise-images/73/Dumbbell-decline-flys-1.png",
        "exercise": 73
    },
    {
        "image": "https://wger.de/media/exercise-images/113/Walking-lunges-2.png",
        "exercise": 113
    },
    {
        "image": "https://wger.de/media/exercise-images/113/Walking-lunges-3.png",
        "exercise": 113
    },
    {
        "image": "https://wger.de/media/exercise-images/113/Walking-lunges-4.png",
        "exercise": 113
    },
    {
        "image": "https://wger.de/media/exercise-images/119/seated-barbell-shoulder-press-large-2.png",
        "exercise": 119
    },
    {
        "image": "https://wger.de/media/exercise-images/123/dumbbell-shoulder-press-large-2.png",
        "exercise": 123
    },
    {
        "image": "https://wger.de/media/exercise-images/152/seated-shoulder-press-machine-large-2.png",
        "exercise": 152
    },
    {
        "image": "https://wger.de/media/exercise-images/148/lateral-dumbbell-raises-large-1.png",
        "exercise": 148
    },
    {
        "image": "https://wger.de/media/exercise-images/154/lying-leg-curl-machine-large-2.png",
        "exercise": 154
    },
    {
        "image": "https://wger.de/media/exercise-images/117/seated-leg-curl-large-2.png",
        "exercise": 117
    },
    {
        "image": "https://wger.de/media/exercise-images/118/standing-leg-curls-large-2.png",
        "exercise": 118
    },
    {
        "image": "https://wger.de/media/exercise-images/22/lying-leg-curl-machine-large-2.png",
        "exercise": 22
    },
    {
        "image": "https://wger.de/media/exercise-images/133/seated-leg-curl-large-2.png",
        "exercise": 133
    },
    {
        "image": "https://wger.de/media/exercise-images/231/Standing-biceps-curl-2.png",
        "exercise": 231
    },
    {
        "image": "https://wger.de/media/exercise-images/232/Triceps-kickback-2.png",
        "exercise": 232
    },
    {
        "image": "https://wger.de/media/exercise-images/228/Arnold-press-1.png",
        "exercise": 228
    },
    {
        "image": "https://wger.de/media/exercise-images/53/Shoulder-press-machine-1.png",
        "exercise": 53
    },
    {
        "image": "https://wger.de/media/exercise-images/241/Dumbbell-shoulder-press-1.png",
        "exercise": 241
    },
    {
        "image": "https://wger.de/media/exercise-images/266/Seated-military-shoulder-press-1.png",
        "exercise": 266
    },
    {
        "image": "https://wger.de/media/exercise-images/20/Dumbbell-lateral-raises-2_LxObAQ4.png",
        "exercise": 20
    },
    {
        "image": "https://wger.de/media/exercise-images/47/Lying-rear-lateral-raise-1.png",
        "exercise": 47
    },
    {
        "image": "https://wger.de/media/exercise-images/242/Incline-biceps-curl-2.png",
        "exercise": 242
    },
    {
        "image": "https://wger.de/media/exercise-images/13/Standing-calf-raises-2.gif",
        "exercise": 13
    },
    {
        "image": "https://wger.de/media/exercise-images/244/Close-grip-front-lat-pull-down-1.png",
        "exercise": 244
    }
];


const getImage = (exerciseNumber) => {
    let temp = [];
    images.forEach((elem) => {
        if(elem.exercise === exerciseNumber){
            temp.push(elem.image);
        }
    });
    return temp.length === 0 ? null : temp;
}
export default getImage;