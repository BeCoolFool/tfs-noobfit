const exercises = [{
        "id": 436,
        "description": "<p>1 Minute for each exercise</p>\n<ol>\n<li>Hold feet 6 inches off ground</li>\n<li>Crunches</li>\n<li>Side Crunches (L)</li>\n<li>Side Crunches (R)</li>\n<li>Heel Touches</li>\n<li>Plank Crunch</li>\n<li>Scissor Kicks</li>\n<li>Swim Kicks</li>\n<li>V Crunches</li>\n<li>Hold feet 6 in off ground</li>\n</ol>\n<p>Exercises can be substituted to vary workout</p>",
        "name": "10 Min Abs",
        "name_original": "10 Min Abs",
        "uuid": "3c5f6e1c-cb22-4a9f-a13e-d14afeb29175",
    },
    {
        "id": 607,
        "description": "<p>Engage abs, then alternate between tapping your left and right ankle by reaching either side with your arm extended.</p>",
        "name": "Ankle Taps",
        "name_original": "Ankle Taps",
        "uuid": "0d9a3d8c-ee81-4b21-9cd0-7d1c9e58cf80",
    },
    {
        "id": 410,
        "description": "<p>Opposite of hollow hold. Lie on stomach with arms and legs extended. See https://www.youtube.com/watch?v=44ScXWFaVBs&amp;feature=youtu.be&amp;t=7m51s</p>",
        "name": "Arch Hold",
        "name_original": "Arch Hold",
        "uuid": "3ec8f0a8-339b-419f-b03d-f9e215834a63",
    },

    {
        "id": 227,
        "description": "<p>Very common shoulder exercise.</p>\n<p> </p>\n<p>As shown here: https://www.youtube.com/watch?v=vj2w851ZHRM</p>",
        "name": "Arnold Shoulder Press",
        "name_original": "Arnold Shoulder Press",
        "uuid": "53ca25b3-61d9-4f72-bfdb-492b83484ff5",

    },
    {
        "id": 352,
        "description": "<ol>\n<li>single leg sit and stand</li>\n</ol>",
        "name": "Asai Squad",
        "name_original": "Asai Squad",
        "uuid": "bad296f4-35a5-4125-ae3f-0187f45945e2",
    },
    {
        "id": 353,
        "description": "<ol>\n<li>single leg sit and stand</li>\n</ol>",
        "name": "Asai Squad",
        "name_original": "Asai Squad",
        "uuid": "583281c7-2362-48e7-95d5-8fd6c455e0fb",
    },
    {
        "id": 289,
        "description": "<p>Grab dumbbells and extend arms to side and hold as long as you can</p>",
        "name": "Axe Hold",
        "name_original": "axe hold",
        "uuid": "6add5973-86d0-4543-928a-6bb8b3f34efc",
    },
    {
        "id": 473,
        "description": "<p>Perform eveyday.  90 minute wokout.  </p>",
        "name": "Back, Shoulder, And Leg Stretching.",
        "name_original": "Back, shoulder, and leg stretching.",
        "uuid": "d7562a07-c9d4-4b03-9724-4ff7d754c980",
    },
    {
        "id": 637,
        "description": "Place a barbell in a rack just below shoulder-height. Dip under the bar to put it behind the neck across the top of the back, and grip the bar with the hands wider than shoulder-width apart. Lift the chest up and squeeze the shoulder blades together to keep the straight back throughout the entire movement. Stand up to bring the bar off the rack and step backwards, then place the feet so that they are a little wider than shoulder-width apart. Sit back into hips and keep the back straight and the chest up, squatting down so the hips are below the knees. From the bottom of the squat, press feet into the ground and push hips forward to return to the top of the standing position.",
        "name": "Back Squat",
        "name_original": "Back Squat",
        "uuid": "0fd6154d-fb53-4b24-acc0-1c5c05b57ebc",
    },
    {
        "id": 343,
        "description": "<p>Place a barbell on the floor at your feet.</p>\n<p>Bending at the waist, grip the barbell with a shoulder with overhand grip.</p>\n<p>With a slow controlled motion, roll the bar out so that your back is straight.</p>\n<p>Roll back up raising your hips and butt as you return to the starting position.</p>",
        "name": "Barbell Ab Rollout",
        "name_original": "Barbell Ab Rollout",
        "uuid": "1b9dc5bc-790b-4e21-a55d-f8b3115e94c5",
    },
    {
        "id": 581,
        "description": "Bench pressing heavy things using heavy objects",
        "name": "Barbell Bench Press",
        "name_original": "Barbell Bench Press",
        "uuid": "fee2c240-9890-4de0-9395-b481eda3bf2d",
    },
    {
        "id": 407,
        "description": "<p>Perform leg squats with barbell behind your legs</p>",
        "name": "Barbell Hack Squats",
        "name_original": "Barbell Hack Squats",
        "uuid": "1215dad0-b7e0-42c6-80d4-112f69acb68a",
    },
    {
        "id": 461,
        "description": "<p><strong>Preparation</strong></p>\n<p> </p>\n\nSit on floor with long side of bench behind back. Roll barbell back and center over hips. Position upper back on corner of bench. Place feet on floor approximately shoulder width with knees bent. Grasp bar to each side.\n\n<p> </p>\n<p><strong>Execution</strong></p>\n<p> </p>\n\nRaise bar upward by extending hips until straight. Lower and repeat.\n",
        "name": "Barbell Hip Thrust",
        "name_original": "Barbell Hip Thrust",
        "uuid": "c96debf3-b245-48f4-977f-b8f79e2b0895",
    },
    {
        "id": 658,
        "description": "<ol>\n<li>With a loaded barbell on the long side of the bench, sit on the floor so your legs are straight, and roll the barbell over your legs up to your hip crease. Make sure your back is against the bench. </li>\n<li>Then, grab the barbell and plant your feet flat on the floor.</li>\n<li>Thrust your hips up so the bar is on your hip crease and lean your upper back onto the bench.</li>\n<li>Now, lower your butt down to a few inches from the floor.</li>\n<li>Then, thrust your hips back up while squeezing your glutes.</li>\n<li>Complete the preferred number of reps.</li>\n</ol>",
        "name": "Barbell Hip Thrust",
        "name_original": "Barbell Hip Thrust",
        "uuid": "0f2c5bc9-4dde-4160-a693-704bd2bd029c",
    },
    {
        "id": 405,
        "description": "<p>Put barbell on the back of your shoulders. Stand upright, then take the first step forward. Step should bring you forward so that your supporting legs knee can touch the floor. Then stand back up and repeat with the other leg.</p>\n<p>Remember to keep good posture.</p>",
        "name": "Barbell Lunges",
        "name_original": "Barbell Lunges",
        "uuid": "ae6a6c23-4616-49b7-a152-49d7461c2b7f",
    },
    {
        "id": 664,
        "description": "<p>You start standing with the bar held in front of you, resting on your deltoids/clavicle and lightly pressing the neck. Take a step back with one leg and lunge. (Don't let your knee touch the ground; this usually is quite bad for your knee and will cause pain later. ) Squeeze the glutes and quads as you press upward from the toes back to a standing position with both legs side by side. Repeat on other side.</p>",
        "name": "Barbell Reverse Lunge - Clean Grip",
        "name_original": "Barbell Reverse Lunge - Clean Grip",
        "uuid": "f36d6917-57e1-454c-a304-ded4c4f0867e",
    },
    {
        "id": 522,
        "description": "This is the beset way to do a squat. Just do it and you are good to go",
        "name": "Barbell Squat",
        "name_original": "barbell squat",
        "uuid": "4e347281-048b-48e8-a8f3-93510e0668cc",
    },
    {
        "id": 344,
        "description": "<p>Position barbell overhead with narrow overhand grip.</p>\n<p>Lower forearm behind upper arm with elbows remaining overhead. Extend forearm overhead. Lower and repeat.</p>",
        "name": "Barbell Triceps Extension",
        "name_original": "Barbell Triceps Extension",
        "uuid": "2cd5e256-20a7-4bc8-a7a8-d62bf8ce00cf",
    },
    {
        "id": 451,
        "description": "<p>With forearms horizontal and stationary, palms up, curl wrists up then return to down position, repeat</p>",
        "name": "Barbell Wrist Curls",
        "name_original": "Barbell Wrist Curls",
        "uuid": "7d81842a-2a9a-48e6-8445-decd03fc1f46",
    },
    {
        "id": 665,
        "description": "Basic crunch: To do a crunch, start by lying on the floor with your knees bent and your feet flat on the ground. Then, cross your arms over your chest. When you're ready, lift your shoulders off the mat while contracting your abs and exhaling. Hold this pose for 1-2 seconds, then inhale and slowly lower back down.",
        "name": "Basic Crunch",
        "name_original": "Basic Crunch",
        "uuid": "eb5dce9c-618d-4041-b45b-278fb2ab9302",
    },
    {
        "id": 666,
        "description": "Basic crunch: To do a crunch, start by lying on the floor with your knees bent and your feet flat on the ground. Then, cross your arms over your chest. When you're ready, lift your shoulders off the mat while contracting your abs and exhaling. Hold this pose for 1-2 seconds, then inhale and slowly lower back down.",
        "name": "Basic Crunch",
        "name_original": "Basic Crunch",
        "uuid": "318fe599-7671-4183-b134-a3094466b8b4",
    },
    {
        "id": 307,
        "description": "<p>-Rest your weight on your palms and the balls of your feet, not dissimilar to normal pushup position</p>\n<p>-Move by stepping with your R palm and L foot, then your L palm and R foot.  Basically, walk like a lumbering bear.</p>\n<p>-Move as fast as you can.  Measure your reps/sets in either distance (i.e. 40 yards) or time (i.e. 45 seconds)</p>\n<p>-Works your Pecs, Deltoids, Triceps, Traps, Lats, Abs and Lower Back, Hip Flexors, Quads, Glutes and Calves</p>",
        "name": "Bear Walk",
        "name_original": "Bear Walk",
        "uuid": "1b8b1657-40fd-4e3b-97b7-1c79b1079f8e",
    },
    {
        "id": 388,
        "description": "<p>-Rest your weight on your palms and the balls of your feet, not dissimilar to normal pushup position</p>\n<p>-Move by stepping with your R palm and L foot, then your L palm and R foot.  Basically, walk like a lumbering bear.</p>\n<p>-Move as fast as you can.  Measure your reps/sets in either distance (i.e. 40 yards) or time (i.e. 45 seconds)</p>\n<p>-Works your Pecs, Deltoids, Triceps, Traps, Lats, Abs and Lower Back, Hip Flexors, Quads, Glutes and Calves</p>",
        "name": "Bear Walk",
        "name_original": "Bear Walk",
        "uuid": "79181b05-1429-45be-bc8a-5bf0913d22b9",
    },
    {
        "id": 192,
        "description": "<p>Lay down on a bench, the bar should be directly above your eyes, the knees are somewhat angled and the feet are firmly on the floor. Concentrate, breath deeply and grab the bar more than shoulder wide. Bring it slowly down till it briefly touches your chest at the height of your nipples. Push the bar up.</p>\n<p>If you train with a high weight it is advisable to have a <em>spotter</em> that can help you up if you can't lift the weight on your own.</p>\n<p>With the width of the grip you can also control which part of the chest is trained more:</p>\n<ul>\n<li>wide grip: outer chest muscles</li>\n<li>narrow grip: inner chest muscles and triceps</li>\n</ul>",
        "name": "Bench Press",
        "name_original": "Bench Press",
        "uuid": "5da6340b-22ec-4c1b-a443-eef2f59f92f0",
    },
    {
        "id": 97,
        "description": "<p>The movement is very similar to benchpressing with a barbell, however, the weight is brought down to the chest at a lower point.</p>\n<p>Hold two dumbbells and lay down on a bench. Hold the weights next to the chest, at the height of your nipples and press them up till the arms are stretched. Let the weight slowly and controlled down.</p>",
        "name": "Benchpress Dumbbells",
        "name_original": "Benchpress dumbbells",
        "uuid": "0ec76f5d-1311-4d6d-bf79-00fa17c3061a",
    },
    {
        "id": 88,
        "description": "<p>Lay down on a bench, the bar is directly over your eyes, the knees form a slight angle and the feet are firmly on the ground. Hold the bar with a narrow grip (around 20cm.). Lead the weight slowly down till the arms are parallel to the floor (elbow: right angle), press then the bar up. When bringing the bar down, don't let it down on your nipples as with the regular bench pressing, but somewhat lower.</p>",
        "name": "Bench Press Narrow Grip",
        "name_original": "Bench press narrow grip",
        "uuid": "03d821e7-e1ac-4026-903b-d406381cbf76",
    },
    {
        "id": 268,
        "description": "<p>Bend over slightly while holding two dumbbells.  Pull the dumbbells up to your chest, keeping your elbows as high as you can.</p>",
        "name": "Bent High Pulls",
        "name_original": "Bent High Pulls",
        "uuid": "34ce7ec5-12a1-4bf8-b4f1-94544c3f1cfc",
    },
    {
        "id": 412,
        "description": "<ol>\n<li>Holding a barbell with a pronated grip (palms facing down), bend your knees slightly and bring your torso forward, by bending at the waist, while keeping the back straight until it is almost parallel to the floor. Tip: Make sure that you keep the head up. The barbell should hang directly in front of you as your arms hang perpendicular to the floor and your torso. This is your starting position.</li>\n<li>Now, while keeping the torso stationary, breathe out and lift the barbell to you. Keep the elbows close to the body and only use the forearms to hold the weight. At the top contracted position, squeeze the back muscles and hold for a brief pause.</li>\n<li>Then inhale and slowly lower the barbell back to the starting position.</li>\n<li>Repeat for the recommended amount of repetitions.</li>\n</ol>",
        "name": "Bent Over Barbell Row",
        "name_original": "Bent Over Barbell Row",
        "uuid": "b8466ace-ebc7-42bb-941c-39e0e7e67a4f",
    },
    {
        "id": 362,
        "description": "<p>With dumbbells in hand, bend at the hip until hands hang just below the knees (similar to straight-legged-deadlift starting position). Keep upper body angle constant while contracting your lats to pull you ellbows back pinching the shoulder blades at the top. Try not to stand up with every rep, check hands go below knees on every rep.</p>",
        "name": "Bentover Dumbbell Rows",
        "name_original": "Bentover Dumbbell Rows",
        "uuid": "f866fd75-c18d-4419-84b9-1b9ccd22adba",
    },
    {
        "id": 421,
        "description": "<p>Sit on bench while holding weights. Bend forward as far as possible, with arms slightly bent at the elbow. Perform a lateral raise while maintaining the bend in your elbow.</p>",
        "name": "Bent-over Lateral Raises",
        "name_original": "Bent-over Lateral Raises",
        "uuid": "170cc52f-345f-41b3-bdae-8a5e0c9aa449",
    },
    {
        "id": 109,
        "description": "<p>Grab the barbell with a wide grip (slightly more than shoulder wide) and lean forward. Your upper body is not quite parallel to the floor, but forms a slight angle. The chest's out during the whole exercise.</p>\n<p>Pull now the barbell with a fast movement towards your belly button, not further up. Go slowly down to the initial position. Don't swing with your body and keep your arms next to your body.</p>",
        "name": "Bent Over Rowing",
        "name_original": "Bent over rowing",
        "uuid": "f82c579e-c069-4dc7-8e36-a3266dfd8e4a",
    },
    {
        "id": 110,
        "description": "<p>The same as <em>regular</em> rowing, but holding a reversed grip (your palms pointing forwards):</p>\n<p>Grab the barbell with a wide grIp (slightly more than shoulder wide) and lean forward. Your upper body is not quite parallel to the floor, but forms a slight angle. The chest's out during the whole exercise.</p>\n<p>Pull now the barbell with a fast movement towards your belly button, not further up. Go slowly down to the initial position. Don't swing with your body and keep your arms next to your body.</p>",
        "name": "Bent Over Rowing Reverse",
        "name_original": "Bent over rowing reverse",
        "uuid": "77688310-f2cf-4d37-a854-dca906387be7",
    },
    {
        "id": 74,
        "description": "<p>Hold the Barbell shoulder-wide, the back is straight, the shoulders slightly back, the arms are streched. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement.</p>\n<p>Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).</p>",
        "name": "Biceps Curls With Barbell",
        "name_original": "Biceps curls with barbell",
        "uuid": "c56078d2-ae85-4524-a467-d1e143b6df1a",
    },
    {
        "id": 669,
        "description": "<p>Hold two barbells, the arms are streched, the hands are on your side, the palms face inwards. Bend the arms and bring the weight with a fast movement up. At the same time, rotate your arms by 90 degrees at the very beginning of the movement. At the highest point, rotate a little the weights further outwards. Without a pause, bring them down, slowly.</p>\n<p>Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).</p>",
        "name": "Biceps Curls With Dumbbell",
        "name_original": "Biceps curls with dumbbell",
        "uuid": "e60ba0d0-30ad-43aa-af28-6c97748da6ca",
    },
    {
        "id": 80,
        "description": "<p>Hold the SZ-bar shoulder-wide, the back is straight, the shoulders slightly back, the arms are streched. Bend the arms, bringing the weight up, with a fast movement. Without pausing, let down the bar with a slow and controlled movement.</p>\n<p>Don't allow your body to swing during the exercise, all work is done by the biceps, which are the only mucles that should move (pay attention to the elbows).</p>",
        "name": "Biceps Curls With SZ-bar",
        "name_original": "Biceps curls with SZ-bar",
        "uuid": "38919515-ce04-4383-9c3f-5846edd0e844",
    },
    {
        "id": 129,
        "description": "<p>Stand around 30 - 40cm away from the cable, the feet are firmly on the floor. Take the bar and lift the weight with a fast movements. Lower the weight as with the dumbbell curls slowly and controlled.</p>",
        "name": "Biceps Curl With Cable",
        "name_original": "Biceps curl with cable",
        "uuid": "8c6c1544-cbf8-403c-ae12-b27b392702f8",
    },
    {
        "id": 530,
        "description": "<p> </p>\n<p>Kneel on floor with your hands placed about shoulder width apart.</p>\n<p> </p>\n<p>Point one arm out straight in front and extend the opposite leg to the rear. The shoulders and the hips should be parallel.</p>\n<p> </p>\n<p>Hold for 10 seconds and return to ground position.</p>\n<p> </p>\n<p>Start with 5 reps on alternate hands and knees, 10  in total. Add up to 3 sets of 10.</p>\n<p> </p>\n<p>As a variation, you can do several bird dogs with one side and then do a set with just the other side.</p>\n<p> </p>\n<p>Keep the abs engaged while you change sides. Work to minimize any extra motion during the weight shift.</p>\n<p> </p>",
        "name": "Bird Dog",
        "name_original": "Bird Dog",
        "uuid": "7b9b9e82-570d-4a0b-8727-054ed1cbc995",
    },

    {
        "id": 341,
        "description": "<ol>\n<li>Assume a plank position on the ground. You should be supporting your bodyweight on your toes and forearms, keeping your torso straight. Your forearms should be shoulder-width apart. This will be your starting position.</li>\n<li>Pressing your palms firmly into the ground, extend through the elbows to raise your body from the ground. Keep your torso rigid as you perform the movement.</li>\n<li>Slowly lower your forearms back to the ground by allowing the elbows to flex.</li>\n<li>Repeat as needed.</li>\n</ol>",
        "name": "Body-Ups",
        "name_original": "Body-Ups",
        "uuid": "4e15a410-5530-4b86-964f-8af26e118e2b",
    },
    {
        "id": 604,
        "description": "<p> </p>\n<p>Bodyweight Squats are a simple exercise that requires no equipment what soever (Though there are variants that add weights).</p>\n<p> </p>\n<ol>\n<li>\n<p>Setup by standing up straight and placing your feet slightly further then shoulder width apart with your feet pointing slightly outwards. Hold your arms straight out infront.</p>\n</li>\n<li>\n<p>'squat' down leading with your hips as if you are going to sit down on a chair, untill your hip is parallel with or lower then your knees. While doing this you should keep your back straight and your body weight should be towards the back of your feet. Your knees shouldn't should end up behind your toes. If they are in front of them you are not leading the squat with your hips and will have the wrong posture.</p>\n</li>\n<li>\n<p>Stand back up and repeat as necessary.</p>\n</li>\n</ol>\n<p> </p>\n<p> </p>\n<p>Holding your arms straight out in front of you will help you keep the correct posture through out the exercise. It's also helpful in maintaining posture to choose a point on the wall to focus on.</p>\n<p></p>\n<p></p>",
        "name": "Bodyweight Squats",
        "name_original": "Bodyweight Squats",
        "uuid": "1d60b6ef-8017-4789-a982-94e0c9e3adef",
    },

    {
        "id": 452,
        "description": "<p>Create a circuit on an overhanging bouldering wall. The circuit should be between 20 and 60 moves depending on the type of climb you are training for. The difficulty should be at a level that you are failing near the end or only just finishing the circuit on your final lap. Ideally the moves are sustained (so no hard crux, unless that is specific to your goal climb) and on similar style of holds to your goal climb.</p>",
        "name": "Boulder Circuit",
        "name_original": "Boulder circuit",
        "uuid": "28150a9f-5533-473f-8072-ddfce3581503",
    },
    {
        "id": 342,
        "description": "<p>Stand with feet slightly wider than shoulder-width apart, while standing as tall as you can.</p>\n<p>Grab a weight plate and hold it out in front of your body with arms straight out. Keep your core tight and stand with a natural arch in your back.</p>\n<p>Now, push hips back and bend knees down into a squat as far as you can. Hold for a few moments and bring yourself back up to the starting position.</p>",
        "name": "Braced Squat",
        "name_original": "Braced Squat",
        "uuid": "5bd3ba00-8f4e-4935-a682-87b150d830c8",
    },
    {
        "id": 346,
        "description": "<p>Rest back foot on an elevated surface with sole pointing up. keep your weight over your front leg as you lower yourself</p>",
        "name": "Bulgarian Split Squat",
        "name_original": "Bulgarian Split Squat",
        "uuid": "1d90f3a8-56e4-4c15-a4b4-94fc0e114e8c",
    },
    {
        "id": 354,
        "description": "<p>Jump, lay down on your chest, do a pushup then jump, repeat</p>",
        "name": "Burpees",
        "name_original": "Burpees",
        "uuid": "6335de72-146f-4f38-886d-6b8a27db62ff",
    },
    {
        "id": 98,
        "description": "<p>Sit on the butterfly machine, the feet have a good contact with the floor, the upper arms are parallel to the floor. Press your arms together till the handles are practically together (but aren't!). Go slowly back. The weights should stay all the time in the air.</p>",
        "name": "Butterfly",
        "name_original": "Butterfly",
        "uuid": "3c3857f8-d224-4d5a-8cc1-f4e7982d3475",
    },
    {
        "id": 99,
        "description": "<p>The movement is the same as with a regular butterfly, only that the grip is narrow:</p>\n<p>Sit on the butterfly machine, the feet have a good contact with the floor, the upper arms are parallel to the floor. Press your arms together till the handles are practically together (but aren't!). Go slowly back. The weights should stay all the time in the air.</p>",
        "name": "Butterfly Narrow Grip",
        "name_original": "Butterfly narrow grip",
        "uuid": "08637e04-d995-4c07-b021-a20f26b6fd97",
    },
    {
        "id": 124,
        "description": "The movement is the same as with a regular butterfly, only that sitting backwards.",
        "name": "Butterfly Reverse",
        "name_original": "Butterfly reverse",
        "uuid": "8715a96e-c8a2-458a-b023-4ea7d82fdab8",
    },
    {
        "id": 646,
        "description": "<p>This is a variant of the twisting Low-to-High Cable Chopper. </p>",
        "name": "Cable Chopper - High-to-Low",
        "name_original": "Cable Chopper - High-to-Low",
        "uuid": "c1cf824a-c094-49b3-8690-966d26ef4e9a",
    },
    {
        "id": 207,
        "description": "<p>Begin with cables at about shoulder height, one in each hand. Take a step forward so that one foot is in front of the other, for stability, and so that there is tension on the cables. Bring hands together in front of you. Try to make your hands overlap (so that the cables cross) a few inches.</p>",
        "name": "Cable Cross-over",
        "name_original": "Cable cross-over",
        "uuid": "1b9ed9da-46d1-4484-8acc-236379ee823c",
    },
    {
        "id": 265,
        "description": "<p>Steps:</p>\n<ol>\n<li>Start off placing an extension band around a post or in a secure position where it will not release and is at elbow level.</li>\n<li>Position yourself to the side of the band and with your hand that is opposite of the band, reach out and grab the handle.</li>\n<li>Bring the band to your chest keeping your elbow bent in a 90 degree angle then slowly rotate your arm in a backhand motion so that the band externally rotates out</li>\n<li>Continue out as far as possible so that you feel a stretch in your shoulders, hold for a count and then return back to the starting position.</li>\n<li>Repeat for as many reps and sets as desired.</li>\n</ol>",
        "name": "Cable External Rotation",
        "name_original": "Cable External Rotation",
        "uuid": "a7e15f4f-a7e6-4c0c-9bcb-ef3de90201aa",
    },
    {
        "id": 428,
        "description": "\nFace high pulley and grasp rope attachment with clinched hands side by side (palms in). Position elbows to side.\n\n\nExtend arms down. Turn palms down at bottom. Return until forearm is close to upper arm and hands are in original position. Repeat.\n",
        "name": "Cable Pushdown (with Rope Attachment)",
        "name_original": "Cable pushdown (with rope attachment)",
        "uuid": "e34b8c35-ef08-453f-93e9-088a9a7134c2",
    },
    {
        "id": 563,
        "description": "<p>Standard shoulder shrug using the cable system.</p>",
        "name": "Cable Shrug",
        "name_original": "Cable Shrug",
        "uuid": "df2dc04f-e7e0-4eb9-9db0-efc63f0ba95d",
    },
    {
        "id": 167,
        "description": "<p>Set cable pulley slightly lower than chest height. Keep body facing forward with hips stable.  Grab the pulley handle, fully extend your arms and bring your arms forward and across your body. Hold for 1 second at the end of the movement and slowly return to starting position.</p>",
        "name": "Cable Woodchoppers",
        "name_original": "Cable Woodchoppers",
        "uuid": "c376346a-1923-4468-9320-b0f480c46b1e",
    },
    {
        "id": 308,
        "description": "<p>Put  the balls of your feet on an extended leg press pad.  Use your calves to press the weight by flexing your feet/toes into a pointed position, and releasing back into a relaxed position.</p>\n<p>This exercise builds mass and strength in the Gastrocnemius and Soleus muscles as well, if not better, than any calf exercise.</p>",
        "name": "Calf Press Using Leg Press Machine",
        "name_original": "Calf Press using Leg Press Machine",
        "uuid": "074530d6-801a-404b-b3b7-adc207be69be",
    },
    {
        "id": 104,
        "description": "<p>Place yourself on the machine with your back firmly against the backrest, the feet are on the platform for calf raises. Check that the feet are half free and that you can completely stretch the calf muscles down.</p>\n<p>With straight knees pull up your weight as much as you can. Go with a fluid movement down till the calves are completely stretched. Repeat.</p>",
        "name": "Calf Raises on Hackenschmitt Machine",
        "name_original": "Calf raises on Hackenschmitt machine",
        "uuid": "e7677699-5e4f-49e6-a645-e8915a203c4d",
    },
    {
        "id": 290,
        "description": "<p>Place car in neutral (with partner to apply breaks if necessary) and drive with legs to push car</p>",
        "name": "Car Push",
        "name_original": "car push",
        "uuid": "09961ef0-6dbb-4473-b100-43a9b2ba98f8",
    },
    {
        "id": 657,
        "description": "1. Stand facing a chair\n2. Step up onto the chair\n3. Step off the chair4. Repeat\n",
        "name": "Chair Steps",
        "name_original": "Chair steps",
        "uuid": "67371d9a-5c9d-4f5d-ba7a-bb17a11396da",
    },
    {
        "id": 181,
        "description": "<p>Like pull-ups but with a reverse grip</p>",
        "name": "Chin-ups",
        "name_original": "Chin-ups",
        "uuid": "f6c4e2fa-226d-46e8-87dc-75fc8cd628bd",
    },
    {
        "id": 157,
        "description": "<p>Pull up your body with your arms while hangig on the edge.</p>",
        "name": "Chin Ups",
        "name_original": "Chin ups",
        "uuid": "079c2147-1030-4c7d-9cb5-e4f17e7768c8",
    },
    {
        "id": 437,
        "description": "<p>Circuit of the 4 exercises</p>\n<ol>\n<li>Pullups x 20 (assisted if needed)</li>\n<li>Pushups x 30</li>\n<li>Crunches x 40</li>\n<li>Air Squats x 50</li>\n</ol>\n<p>Repeat entire circuit 5 times with a 3 min break in between each set</p>",
        "name": "Circuit - Pullups, Pushups, Crunches, Air Squats",
        "name_original": "Circuit - Pullups, Pushups, Crunches, Air Squats",
        "uuid": "8b7ed43f-461b-48b9-b541-6f028fbca801",
    },
    {
        "id": 217,
        "description": "<p>Perform a typical bench press, but hold the bar with your hands approximately shoulder-width apart and keep your elbows close to your body.</p>",
        "name": "Close-grip Bench Press",
        "name_original": "Close-grip bench press",
        "uuid": "6cc54626-d5ef-4013-9184-010ac96de20a",
    },
    {
        "id": 213,
        "description": "<p>Grip the pull-down bar with your hands closer than shoulder width apart, with your palms facing away from you. Lean back slightly. Pull the bar down towards your chest, keeping your elbows close to your sides as you come down. Pull your shoulders back at the end of the motion.</p>",
        "name": "Close-grip Lat Pull Down",
        "name_original": "Close-grip lat pull down",
        "uuid": "42b092ec-80b1-4471-90d1-e462a65ab18c",
    },
    {
        "id": 558,
        "description": "<p>block/spacer betweet the chest and barbel when going down.  touch block and explode up.</p>",
        "name": "Controlled Bench",
        "name_original": "controlled Bench",
        "uuid": "39bbb293-3428-4082-a6d7-0b577e7eb51d",
    },
    {
        "id": 194,
        "description": "<p>Grasp a moderately weighted dumbbell so your palms are flat against the underside of the top plates and your thumbs are around the bar. Lie on your back across a flat bench so only your upper back and shoulders are in contact with the bench. Your feet should be set about shoulder-width apart and your head should hang slightly downward. With the dumbbell supported at arm's length directly about your chest, bend your arms about 15 degrees and keep them bent throughout the movement. Slowly lower the dumbbell backward and downward in a semicircle arc to as low a position as is comfortably possible. Raise it slowly back along the same arc to the starting point, and repeat for the required number of repetitions.</p>",
        "name": "Cross-Bench Dumbbell Pullovers",
        "name_original": "Cross-Bench Dumbbell Pullovers",
        "uuid": "122f5dd3-fc8c-47fe-9a79-1c15cf784a60",
    },
    {
        "id": 470,
        "description": "<p>Start standing up, put one leg in front of you so you stand on your knee. Twist your core to the same side as your knee. Repeat with the other leg</p>",
        "name": "Crossover Reverse Lunge",
        "name_original": "Crossover Reverse Lunge",
        "uuid": "f0f21f60-43d9-4e8b-a063-73d61de9e63c",
    },
    {
        "id": 564,
        "description": "<p>This is performed while lying face up on the floor with knees bent, by curling the shoulders up towards the pelvis. This is an isolation exercise for the abdominals.</p>",
        "name": "Crunch",
        "name_original": "Crunch",
        "uuid": "61ff25de-d17b-43ab-9d64-9e72ca514bda",
    },
    {
        "id": 91,
        "description": "<p>Lay down on your back a soft surface, the feet are on the floor. Ask a partner or use some other help (barbell, etc.) to keep them fixed, your hands are behind your head. From this position move your upper body up till your head or elbows touch your knees. Do this movement by rolling up your back.</p>",
        "name": "Crunches",
        "name_original": "Crunches",
        "uuid": "d325dd5c-6833-41c7-8eea-6b95c4871133",
    },

    {
        "id": 94,
        "description": "<p>The procedure is very similar as for regular crunches, only with the additional weight of the machine. Sit on the machine, put both feet firmly on the ground. Grab the to the weights, cables, etc. and do a rolling motion forwards (the spine should ideally lose touch vertebra by vertebra). Slowly return to the starting position. </p>",
        "name": "Crunches on Machine",
        "name_original": "Crunches on machine",
        "uuid": "6709577b-95ec-4053-a822-d5fe1f753966",
    },
    {
        "id": 92,
        "description": "<p>Take the cable on your hands and hold it next to your temples. Knee down and hold your upper body straight and bend forward. Go down with a fast movement, rolling your back in (your ellbows point to your knees). Once down, go slowly back to the initial position.</p>",
        "name": "Crunches With Cable",
        "name_original": "Crunches with cable",
        "uuid": "8d6c13c6-256d-4137-b1c6-b0e817697639",
    },
    {
        "id": 416,
        "description": "<p>On your back, legs extended straight up, reach toward your toes with your hands and lift your shoulder blades off the ground and back.</p>",
        "name": "Crunches With Legs Up",
        "name_original": "Crunches with Legs Up",
        "uuid": "6ec0d447-1dfa-40fa-9fad-9a86c222d1a4",

    },
    {
        "id": 543,
        "description": "The core plays a big role in helping you move your limbs while stabilizing your spine—an incredibly important prerequisite for most strength-training exercises,\" Dunham says. The deadbug protects your lower back mid-movement and keeps you from wasting any energy.Lie on your back, with your hips and knees bent to 90°. Raise both arms toward the ceiling. Pull your lower back to the floor to eliminate the gap. Start by pressing one leg out, and tapping the heel to the floor. \"As you extend one leg, exhale as much as you can, keeping your lower back glued to the floor,\" Dunham says. When you can’t exhale any more, pull your knee back to the starting position. Make this more difficult by holding weight in your hands, or by lowering opposite arm and leg.",
        "name": "Deadbug",
        "name_original": "deadbug",
        "uuid": "d846f8c6-4f70-46fc-9b65-600081522936",
    },
    {
        "id": 347,
        "description": "<p>Deadhang performed on an edge either with or without added weight (adujst edge or weight to adjust difficulty)</p>",
        "name": "Deadhang",
        "name_original": "Deadhang",
        "uuid": "8976d255-d7e4-46c7-8079-ca525f0a7d0d",
    },
    {
        "id": 105,
        "description": "<p>Stand firmly, with your feet slightly more than shoulder wide apart. Stand directly behind the bar where it should barely touch your shin, your feet pointing a bit out. Bend down with a straight back, the knees also pointing somewhat out. Grab the bar with a shoulder wide grip, one underhand, one reverse grip.</p>\n<p>Pull the weight up. At the highest point make a slight hollow back and pull the bar back. Hold 1 or 2 seconds that position. Go down, making sure the back is not bent. Once down you can either go back again as soon as the weights touch the floor, or make a pause, depending on the weight.</p>",
        "name": "Deadlifts",
        "name_original": "Deadlifts",
        "uuid": "22cca8fc-cfaf-4941-b0f7-faf9f2937c52",
    },
    {
        "id": 100,
        "description": "<p>Lay down on a decline bench, the bar should be directly above your eyes, the knees are somewhat angled and the feet are firmly on the floor. Concentrate, breath deeply and grab the bar more than shoulder wide. Bring it slowly down till it briefly touches your chest at the height of your nipples. Push the bar up.</p>",
        "name": "Decline Bench Press Barbell",
        "name_original": "Decline bench press barbell",
        "uuid": "b72ae8d4-ede6-4480-8fc5-7b80e369f7ed",
    },
    {
        "id": 101,
        "description": "<p>Take two dumbbells and sit on a decline bench, the feet are firmly on the floor, the head is resting the bench. Hold the weights next to the chest, at the height of your nipples and press them up till the arms are stretched. Let the weight slowly and controlled down.</p>",
        "name": "Decline Bench Press Dumbbell",
        "name_original": "Decline bench press dumbbell",
        "uuid": "80d318b3-4b8a-41aa-9c6c-0a2a921fe1e6",
    },
    {
        "id": 323,
        "description": "<ol>\n<li>Lie down on a bench with a barbell resting on your chest. Position your legs so they are secure on the extension of the abdominal bench.</li>\n<li>While inhaling, tighten your abdominals and glutes. Simultaneously curl your torso as you do when performing a sit-up and press the barbell to an overhead position while exhaling. </li>\n<li>Lower your upper body back down to the starting position while bringing the barbell back down to your torso. Remember to breathe in while lowering the body.</li>\n</ol>\n<p>Caution: It is best to have a spotter while performing this exercise so that it is easier to get the barbell and also get rid of it. Also, always start with an empty bar and do not use jerking motions at any time.</p>\n<p> </p>\n<p>Reference: http://www.bodybuilding.com/exercises/detail/view/name/press-sit-up</p>",
        "name": "Decline Press Sit-Up",
        "name_original": "Decline Press Sit-Up",
        "uuid": "392a531f-f0d6-4f65-b890-63776fc6b053",
    },
    {
        "id": 260,
        "description": "<p>With your feet raised approximately 30cm on a platform, align your shoulders, elbows and hands, then perform regular pushups. This emphasises the clavicular fibers of the pectoralis major.</p>",
        "name": "Decline Pushups",
        "name_original": "Decline Pushups",
        "uuid": "36ef5f12-6f77-4754-a926-39915e4b57a5",
    },
    {
        "id": 381,
        "description": "<p>Preparation</p>\n<p>Stand on weight plate, bumper plate, or shallow elevated platform with loaded bar above feet. Squat down and grasp bar with shoulder width or slightly wider overhand or mixed grip.</p>\n<p> </p>\n<p>Execution</p>\n<p>Lift bar by extending hips and knees to full extension. Pull shoulders back at top of lift if rounded. Return weights to floor by bending hips back while allowing knees to bend forward, keeping back straight and knees pointed same direction as feet. Repeat.</p>\n<p> </p>\n<p>Comments</p>\n<p>Throughout lift, keep hips low, shoulders high, arms and back straight. Knees should point same direction as feet throughout movement. Keep bar close to body to improve mechanical leverage. Grip strength and strength endurance often limit ability to perform multiple reps at heavy resistances. Gym chalk, wrist straps, grip work, and mixed grip can be used to enhance grip. Mixed grip indicates one hand holding with overhand grip and other hand holding with underhand grip. Lever barbell jack can be used to lift barbell from floor for easier loading and unloading of weight plates.</p>\n<p>Barbell Deficit Deadlift emphasizes building strength through lowest portion of Deadlift. Target muscle is exercised isometrically. Heavy barbell deadlifts significantly engages Latissmus Dorsi. See Barbell Deficit Deadlift under Gluteus Maximus. Also see Deadlift Analysis.</p>",
        "name": "Deficit Deadlift",
        "name_original": "Deficit Deadlift",
        "uuid": "83b741dc-fd19-4c85-ac0c-b0388c5838e6",
    },

    {
        "id": 329,
        "description": "<p>You sit at the bench press device, back slightly tilted to the back. The bar should be about 20 cm in front of you. Then you push the bar and take it back again, as you would with a bench press.</p>\n<p>In this position you strain your chest muscles a lot less, which is nice if you want to train, but your chest hasn't recovered yet.</p>\n<p>Here's a link to a girl on a machine specialized for this exercise, to give a better description than my failing words above.</p>\n<p>http://www.schnell-online.de/db_imgs/products/img/t-80400.jpg</p>",
        "name": "Diagonal Shoulder Press",
        "name_original": "Diagonal shoulder Press",
        "uuid": "ee291ad5-2064-4d0a-b252-c715f362e035",

    },
    {
        "id": 82,
        "description": "<p>Hold onto the bars at a narrow place (if they are not parallel) and press yourself up, but don't stretch the arms completely, so the muscles stay during the whole exercise under tension. Now bend the arms and go down as much as you can, keeping the elbows always pointing back, At this point, you can make a short pause before repeating the movement.</p>",
        "name": "Dips",
        "name_original": "Dips",
        "uuid": "22879094-ced4-4bd1-81f2-76fdfeb867e6",
    },
    {
        "id": 83,
        "description": "<p>Put two benches so far appart, that you can hold onto one with your hands and are just able to reach the other with your feet. The legs stay during the exercise completely stretched. With your elbows facing back, bend them as much as you can. Push yourself up, but don't stretch out the arms.</p>",
        "name": "Dips Between Two Benches",
        "name_original": "Dips between two benches",
        "uuid": "aa4fcd9b-baee-41cf-b4c5-8462bc43a8be",
    },

    {
        "id": 528,
        "description": "<p>A dragon flag is typically performed lying face-up on a bench or on the ground with your hands grasping a sturdy object behind you for support.</p>\n<p>From here, the objective is to lift your entire body up in a straight line, stacking it vertically over your shoulders, then slowly lower back down until parallel to the ground and repeat.</p>\n<p>The aim is to keep your body straight, so do your best to avoid bending at the hips. Your abs will have to provide extreme stabilization to do so. In fact, you'll also need to engage your lower back, glutes, and other trunk musculature to maintain your form.</p>\n<p>Though the dragon flag emphasizes the abs, it's really a full-body exercise.</p>\n<p>- T-nation</p>",
        "name": "Dragon Flags",
        "name_original": "Dragon Flags",
        "uuid": "a8422025-90d0-4895-b523-1035fa101b87",
    },

    {
        "id": 287,
        "description": "<p>Grab kettlebell and walk while holding it between legs</p>",
        "name": "Duck Walks",
        "name_original": "duck walks",
        "uuid": "bfeccc21-e354-4878-b4d3-a9fcaa680fc4",
    },
    {
        "id": 275,
        "description": "<p>Sit on bench. Grasp dumbbell between feet. Place back of upper arm to inner thigh. Lean into leg to raise elbow slightly.</p>",
        "name": "Dumbbell Concentration Curl",
        "name_original": "Dumbbell Concentration Curl",
        "uuid": "dfa090e4-77ae-40ed-86c0-4696fe93dcf1",
    },
    {
        "id": 300,
        "description": "<p>Grasp dumbbell with both hands at the sides of the upper plates. Hold dumbbell in front of chest, close to torso. Place feet about shoulderwide apart, keep knees slightly bent.</p>\n<p>Squat down until thighs are parallel to floor. Keep back straight, bend and move hips backward to keep knees above feet. Return, keep knees slightly flexed. Repeat.</p>\n<p>Keep bodyweight on heels and look ahead or slightly above to keep back straight.</p>",
        "name": "Dumbbell Goblet Squat",
        "name_original": "Dumbbell Goblet squat",
        "uuid": "2120f390-4a5f-41bc-9139-045581a3a245",
    },
    {
        "id": 298,
        "description": "<p>With elbows back to sides, raise one dumbbell and rotate forearm until forearm is vertical and palm faces shoulder. Lower to original position and repeat with opposite arm. Continue to alternate between sides.</p>",
        "name": "Dumbbell Incline Curl",
        "name_original": "Dumbbell Incline Curl",
        "uuid": "fa56d30a-7a8f-4084-aa68-46bd52f97959",
    },

    {
        "id": 113,
        "description": "<p>Take two dumbbells in your hands, stand straight, feet about shoulder wide. Take one long step so that the front knee is approximately forming a right angle. The back leg is streched, the knee is low but doesn't touch the ground. \"Complete\" the step by standing up and repeat the movement with the other leg.</p>",
        "name": "Dumbbell Lunges Walking",
        "name_original": "Dumbbell lunges walking",
        "uuid": "ffd4ce7e-e14f-49d4-9dc9-dc1362631382",
    },
    {
        "id": 324,
        "description": "<p>1. Stand straight with shoulders and feet wide apart and hold a dumbbell in your left hand.</p>\n<p>2. Starting position: Place your right hand on your waist.</p>\n<p>3. Now, keep your head and back straight and bend only from your waist to your right.</p>\n<p>4. Inhale, and bend as much as you can and hold for a count of two.</p>\n<p>5. Final position: Get back to the start position, exhale as you do so.</p>\n<p> </p>\n<p>6. Now perform the similar steps bending to your left.</p>\n<p> </p>\n<p>Reference : http://workouttrends.com/how-to-do-dumbbell-side-bends</p>",
        "name": "Dumbbell Side Bends",
        "name_original": "Dumbbell Side Bends",
        "uuid": "5399f95f-5fb3-4d5f-a60f-9c7a1d2add2b",
    },
    {
        "id": 584,
        "description": "<p>Keep your arms at a dead hang in front of you.Contract your biceps as you curl the dumbbells up towards your torso.Squeeze at the top of the repetition for one second.Lower the dumbbells in a slow and controlled manner until fully extended.Repeat for the desired number of repetitions.</p>",
        "name": "Dumbbell Spider Curl",
        "name_original": "Dumbbell Spider Curl",
        "uuid": "35e9678d-1d01-41db-98eb-979f7a96f89a",
    },

    {
        "id": 274,
        "description": "<p>Position one dumbbell over head with both hands under inner plate (heart shaped grip).</p>\n<p>With elbows over head, lower forearm behind upper arm by flexing elbows. Flex wrists at bottom to avoid hitting dumbbell on back of neck. Raise dumbbell over head by extending elbows while hyperextending wrists. Return and repeat.</p>",
        "name": "Dumbbell Triceps Extension",
        "name_original": "Dumbbell Triceps Extension",
        "uuid": "3f7b893e-a089-450f-b8cc-ec3ec17d3bf3",
    },
    {
        "id": 435,
        "description": "<p>Romanian Deadlifts with dumbells in each hands instead of a barbell.</p>",
        "name": "Dumbell Romanian Deadlift",
        "name_original": "Dumbell Romanian Deadlift",
        "uuid": "2f25786e-2b51-49df-b01f-d29c07cf8491",

    },

    {
        "id": 394,
        "description": "<p>Attach a rope to a pulley station set at about chest level.</p>\n<p>Step back so you're supporting the weight with arms completely outstretched and assume a staggered (one foot forward) stance. Bend the knees slightly for a stable base.</p>\n<p>Retract the scapulae (squeeze your partner's finger with your shoulder blades) and pull the center of the rope slightly up towards the face. A good cue is to think about pulling the ends of the rope apart, not just pulling back.</p>\n<p>As you near your face, externally rotate so your knuckles are facing the ceiling.</p>\n<p>Hold for one second at the top position and slowly lower.</p>",
        "name": "Facepull",
        "name_original": "Facepull",
        "uuid": "659f3fb9-2370-42fb-9c29-9aaf65c7a7de",
    },
    {
        "id": 286,
        "description": "<p>Grab dumbbells and walk</p>\n<p> </p>",
        "name": "Farmers Walks",
        "name_original": "farmers walks",
        "uuid": "d7a76292-8d6f-4857-9364-682615d781b4",
    },
    {
        "id": 480,
        "description": "1. Position yourself on all fours with shoulders wide apart and palms flat.\n<p>2. Keep your knees, hip wide apart and bend them at a 90-degrees angle.3. Keep your back straight, raise your right thigh and bring it close to your chest as much as you can.4. Then raise the same thigh out to the side by keeping the hips still.5. Now, kick your raised leg slowly in the backward position.6. Now perform these steps with your left leg.</p>",
        "name": "Fire Hydrant",
        "name_original": "Fire Hydrant",
        "uuid": "97f02715-43e9-44bf-b8a7-4aa0b38a4945",

    },

    {
        "id": 303,
        "description": "<p>-Laying on the back, lift your straightened legs from the ground at a 45 degree angle. </p>\n<p>-As your Left foot travels downward and nearly touches the floor, your Right foot should seek to reach a 90 degree angle, or as close to one as possible.</p>\n<p>-Bring your R foot down until it nearly touches the floor, and bring your L foot upwards.  Maintain leg rigidity throughout the exercise.  Your head should stay off the ground, supported by tightened upper abdominals.</p>\n<p>-(L up R down, L down R up, x2)  ^v, v^, ^v, v^ = 1 rep</p>\n<p>-Primarily works the Rectus Abdominus, the hip flexors and the lower back. Secondarily works the Obliques.  Emphasis placed on the lower quadrant of the abs.</p>\n<p> </p>",
        "name": "Flutter Kicks",
        "name_original": "Flutter Kicks",
        "uuid": "ce37341b-379c-4611-ac9f-830c6c3c397a",
    },
    {
        "id": 145,
        "description": "<p>Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.</p>",
        "name": "Fly With Dumbbells",
        "name_original": "Fly with dumbbells",
        "uuid": "754391c6-39d5-4bb6-a311-68a520f6fd3a",
    },
    {
        "id": 146,
        "description": "<p>The exercise is the same as with a regular bench:</p>\n<p>Take two dumbbells and lay on a bench, make sure the feet are firmly on the ground and your back is not arched, but has good contact with the bench. The arms are stretched in front of you, about shoulder wide. Bend now the arms a bit and let them down with a half-circle movement to the side. Without changing the angle of the elbow bring them in a fluid movement back up.</p>",
        "name": "Fly With Dumbbells, Decline Bench",
        "name_original": "Fly with dumbbells, decline bench",
        "uuid": "acf1f2df-46c4-49a3-8e6c-2979ea4204b1",
    },
    {
        "id": 628,
        "description": "<p>Works the wrist flexor muscles on the front of the forearms.</p>",
        "name": "Forearm Curl",
        "name_original": "Forearm Curl",
        "uuid": "2dc91207-08b8-4750-8460-8db1ce7ba7ef",

    },

    {
        "id": 85,
        "description": "<p>Hold the dumbbells and lay down on a flat bench in such a way that around 1/4 of your head is over the edge. Stretch your arms with the weights and bend them so that the dumbbells are lowered (make sure they don't touch each other). Just before they touch your forehead, push them up.</p>\n<p>Pay attention to your elbows and arms: only the triceps are doing the work, the rest of the arms should not move.</p>",
        "name": "French Press (skullcrusher) Dumbbells",
        "name_original": "French press (skullcrusher) dumbbells",
        "uuid": "ee00d53e-4482-44aa-b780-bbc570061841",
    },
    {
        "id": 84,
        "description": "<p>Hold the SZ-bar and lay down on a flat bench in such a way that around 1/4 of your head is over the edge. Stretch your arms with the bar and bend them so that the bar is lowered. Just before it touches your forehead, push it up.</p>\n<p>Pay attention to your elbows and arms: only the triceps are doing the work, the rest of the arms should not move.</p>",
        "name": "French Press (skullcrusher) SZ-bar",
        "name_original": "French press (skullcrusher) SZ-bar",
        "uuid": "ee4a350b-c681-407f-a414-6ec243809ec7",
    },

    {
        "id": 233,
        "description": "<p>To execute the exercise, the lifter stands with their feet shoulder width apart and weights or resistance handles held by their side with a pronated (overhand) grip.</p>\n<p>The movement is to bring the arms up in front of the body to eye level and with only a slight bend in the elbow. This isolates the anterior deltoid muscle (front of the shoulder) and uses the anterior deltoid to lift the weight.</p>\n<p>When lifting it is important to keep the body still so the anterior deltoid is fully utilised; if the weight cannot be lifted by standing still then it is too heavy and a lower weight is needed. It is important to keep a slight bend in the elbow when lifting as keeping the elbow locked will add stress to the elbow joint and could cause injury.</p>\n<p>A neutral grip, similar to that used in the hammer curl, can also be used. With this variation the weight is again raised to eye level, but out to a 45 degree angle from the front of the body. This may be beneficial for those with shoulder injuries, particularly those related to the rotator cuff.</p>",
        "name": "Front Raises",
        "name_original": "Front raises",
        "uuid": "a87de062-2101-4dc4-adda-328b4c722c6d",
    },

    {
        "id": 326,
        "description": "<p>(A) Get in high plank position on your hands and toes.(B) Shift your weight to your left hand as you turn your body to the right; bend your right leg behind you and extend your right arm up. Return to the center and repeat on the opposite side. Continue, alternating sides.<strong>Make it easier:</strong> Don’t raise your arm after you bend your leg behind you.<strong>Make it harder:</strong> Balance with your arm and leg extended for two counts.</p>",
        "name": "Full Sit Outs",
        "name_original": "Full Sit Outs",
        "uuid": "bf9e572d-d138-43e9-a486-a5c6ad9033f8",
    },

    {
        "id": 408,
        "description": "<p>Lie on you back with your hips and knees flexed, feet on the ground. From this position, raise your butt off of the ground to a height where your body makes a straight line from your knees to your shoulders. To make the exercise more intense, you can add weight by letting a barbell rest on your hips as you complete the motion, or you can put your feet on a slightly higher surface such as a step or a bench.</p>",
        "name": "Glute Bridge",
        "name_original": "Glute Bridge",
        "uuid": "97ae7d90-a4b2-472c-bf7b-826df674622c",

    },

    {
        "id": 86,
        "description": "<p>Hold two dumbbells and sit on a bench with a straight back, the shoulders are slightly rolled backwards. Your pals point to your body. Bend the arms and bring the weight up with a fast movement. Don't rotate your hands, as with the curls. Without any pause bring the dumbbell down with a slow, controlled movement.</p>\n<p>Don't swing your body during the exercise, the biceps should do all the work here. The elbows are at your side and don't move.</p>",
        "name": "Hammercurls",
        "name_original": "Hammercurls",
        "uuid": "6dcc9adb-939c-4581-9e44-d0d73753997b",
    },

    {
        "id": 138,
        "description": "<p>Take a cable in your hands (palms parallel, point to each other), the body is straight. Bend the arms and bring the weight up with a fast movement. Without any pause bring it back down with a slow, controlled movement, but don't stretch completely your arms.</p>\n<p>Don't swing your body during the exercise, the biceps should do all the work here. The elbows are at your side and don't move.</p>",
        "name": "Hammercurls on Cable",
        "name_original": "Hammercurls on cable",
        "uuid": "5baf40e5-ea3c-4f8d-b60a-d294ee2de55b",
    },
    {
        "id": 641,
        "description": "<p>Kneel, anchor feet; lean forward to the floor, pull back-up to kneeling position</p>",
        "name": "Hamstring Curls",
        "name_original": "Hamstring Curls",
        "uuid": "453a8d10-b41d-4701-9d96-759d66c10780",
    },

    {
        "id": 166,
        "description": "<p>Hanging from bar or straps, bring legs up with knees extended or flexed</p>",
        "name": "Hanging Leg Raises",
        "name_original": "Hanging Leg Raises",
        "uuid": "5aab5983-55d6-4d4c-a24e-bc18704f50c5",
    },
    {
        "id": 291,
        "description": "<p>Grab two cables stand in the middle so both have tension and hold</p>",
        "name": "Hercules Pillars",
        "name_original": "Hercules pillars",
        "uuid": "3329b890-685a-475d-b8e5-42362711b445",

    },
    {
        "id": 304,
        "description": "<p>-Start with legs slightly wider than shoulder width</p>\n<p>-Drop into a bodyweight squat</p>\n<p>-As you hit the bottom of the squat, explode upwards into a jump while simultaneously tucking your knees into your chest midflight.  Remain tucked until the apex of your jump.</p>\n<p>-Land on both feet, making sure your knees are not locked so as to avoid excessive strain upon your joints.  Collect yourself into the next rep as quickly but under control as possible.</p>",
        "name": "High Knee Jumps",
        "name_original": "High Knee Jumps",
        "uuid": "d79e7078-0dd3-4b49-8947-731e6aaa1de0",

    },
    {
        "id": 432,
        "description": "<p>-Start with legs at a comfortable standing width</p>\n<p>-Run in place, bringing knees to or above waist level</p>",
        "name": "High Knees",
        "name_original": "High Knees",
        "uuid": "e5b6bf59-d933-416e-8228-75c0e7de5dd9",

    },
    {
        "id": 164,
        "description": "<p>Use a light barbell, perform explosive lift up starting from underneath knee cap level. Lift/raise explosively using hips, at shoulder level. Tempo: 2111</p>",
        "name": "High Pull",
        "name_original": "High pull",
        "uuid": "a0d1e216-4517-46db-ac4c-fa11686fd513",

    },

    {
        "id": 650,
        "description": "<p>Start with your feet shoulder width apart and arms slightly behind your back.</p>\n<p>As you descend towards the floor, raise your heels off the ground, while keeping your back as vertical  as possible. </p>\n<p>Upon attaining the bottom position, touch the hands to the heels.</p>\n<p>Then stand up ending with the heels on the ground, arms extended in front of the chest then rowing into the start position.</p>",
        "name": "Hindu Squats",
        "name_original": "Hindu Squats",
        "uuid": "06dee092-b84a-40b4-986f-b0b3e64adc22",

    },
    {
        "id": 376,
        "description": "Lying down on your back, with your feet flat on the floor. Raise your hips up evenly as high as you can and hold for as long as you can.",
        "name": "Hip Raise, Lying",
        "name_original": "Hip Raise, Lying",
        "uuid": "5b12d361-be23-4f61-94af-0cc1665711ca",
    },

    {
        "id": 571,
        "description": "<ol>\n<li>Start with your shoulder blades against a bench, and your arms spread across it for stability.  If your shoulders don’t reach the bench, you may need to start with your butt slightly off the floor.  Bend your knees to about 90 degrees, and make sure your feet are flat on the floor.</li>\n<li>Take a big breath in, blow your air out fully, and brace your core.</li>\n<li>Squeeze your glutes, lift up your hips pushing the barbell up. Hold for a second or two.</li>\n<li>Slowly reslease and repeat.</li>\n</ol>",
        "name": "Hip Thrusts",
        "name_original": "Hip Thrusts",
        "uuid": "479b303d-1c0d-4fa0-9009-7e9b506ead97",
    },
    {
        "id": 383,
        "description": "<p>Get on a mat and lie on your back. Contract your abs, stretch your raise and legs and raise them (your head and shoulders are also be raised). Make sure your lower back remains in contact with the mat.</p>",
        "name": "Hollow Hold",
        "name_original": "Hollow Hold",
        "uuid": "9910d61d-b5d4-44fe-8b5f-00fa6e2aaf76",
    },
    {
        "id": 601,
        "description": "<p>Bend slowly the legs as low as possible and the quadriceps should touch lightly the chest. Press the platform till the starting position. Hold the contraction and after a moment bend the legs again.</p>",
        "name": "Horizontal Leg Press",
        "name_original": "Horizontal Leg Press",
        "uuid": "c6bf23d5-e59a-42a3-95bd-84b6e4dfa110",
    },
    {
        "id": 566,
        "description": "<ul>\n<li>Straight body</li>\n<li>Elbows in</li>\n<li>Arms straight at the bottom</li>\n<li>Rings/bar to chest and shoulder blades fully retracted (pinched together) at the top</li>\n<li>Don't let your shoulders shrug up</li>\n</ul>",
        "name": "Horizontal Rows",
        "name_original": "Horizontal Rows",
        "uuid": "a5db67e8-83ab-46f1-beff-5a56390d786a",
    },

    {
        "id": 128,
        "description": "<p>Lay on the hyperextension pad with the belly button just at the leading edge, the upper body can hang freely. Tense your whole back's muscles and bring your upper body up till it is horizontal, but not more. Go slowly down and don't relax your muscles.</p>",
        "name": "Hyperextensions",
        "name_original": "Hyperextensions",
        "uuid": "f57a0c60-7d37-4eb3-a94e-1a90292e8c02",
    },
    {
        "id": 463,
        "description": "<ol>\n<li>Load the bar to an appropriate weight for your training.</li>\n<li>Lay on the bench with your feet flat on the ground, driving through to your hips. Your back should be arched, and your shoulder blades retracted.</li>\n<li>Take a medium, pronated grip covering the rings on the bar. Remove the bar from the rack, holding the weight above your chest with your arms extended. This will be your starting position.</li>\n<li>Lower the bar to the sternum by flexing the elbows. Maintain control and do not bounce the bar off of your chest. Your lats should stay tight and elbows slightly drawn in.</li>\n<li>After touching your torso with the bar, extend the elbows to return the bar to the starting position.</li>\n</ol>",
        "name": "Incline Barbell Bench Press",
        "name_original": "Incline Barbell Bench Press",
        "uuid": "db79cf4c-2444-4dcb-b090-662584eea46f",

    },

    {
        "id": 206,
        "description": "<p>Use inclined bench. Hold dumbbells straight out to your sides, elbows slightly bent. Bring arms together above you, keeping angle of elbows fixed.</p>",
        "name": "Incline Dumbbell Flye",
        "name_original": "Incline dumbbell flye",
        "uuid": "7327ab51-7073-4c84-9e3f-2e83af40e96c",
    },
    {
        "id": 210,
        "description": "<ul>\n<li>Bench should be angled anywhere from 30 to 45 degrees</li>\n<li>Be sure to press dumbbells straight upward (perpendicular to the floor)</li>\n</ul>",
        "name": "Incline Dumbbell Press",
        "name_original": "Incline dumbbell press",
        "uuid": "15c5dfa0-4103-4515-979d-807eee8faa53",
    },
    {
        "id": 340,
        "description": "<ol>\n<li>Using a neutral grip, lean into an incline bench.</li>\n<li>Take a dumbbell in each hand with a neutral grip, beginning with the arms straight. This will be your starting position.</li>\n<li>Retract the shoulder blades and flex the elbows to row the dumbbells to your side.</li>\n<li>Pause at the top of the motion, and then return to the starting position.</li>\n</ol>",
        "name": "Incline Dumbbell Row",
        "name_original": "Incline Dumbbell Row",
        "uuid": "fbbebbfc-a5b2-4573-8914-147ff60cb7c5",
    },
    {
        "id": 476,
        "description": "<ol>\n<li>Load an appropriate weight onto the pins and adjust the seat for your height. The handles should be near the top of the pectorals at the beginning of the motion. Your chest and head should be up and your shoulder blades retracted. This will be your starting position.</li>\n<li>Press the handles forward by extending through the elbow.</li>\n<li>After a brief pause at the top, return the weight just above the start position, keeping tension on the muscles by not returning the weight to the stops until the set is complete.</li>\n</ol>",
        "name": "Incline Machine Chest Press",
        "name_original": "Incline Machine Chest Press",
        "uuid": "cf9a0cf0-1efc-41ec-9d40-e6e927a94e5c",
    },
    {
        "id": 165,
        "description": "<p>Perform the plank with legs elevated, feet on a gymball. Once stabilised, slowly move one foot sideways off the ball, then make it touch the floor, then come back to starting position. Alternate with the other foot.</p>\n<p>This is a core exercise.</p>",
        "name": "Incline Plank With Alternate Floor Touch",
        "name_original": "Incline Plank with alternate floor touch",
        "uuid": "39c416c8-64db-4330-9d37-c41ec70048fb",
    },
    {
        "id": 168,
        "description": "<p>Regular push with a 30 degree incline.</p>",
        "name": "Incline Pushups",
        "name_original": "Incline Pushups",
        "uuid": "2d728ea9-9e80-4d16-9d6c-801e1e4b4d0b",
    },
    {
        "id": 338,
        "description": "<p>Assume push-up position, with hands slightly wider than shoulder width.</p>\n<p>Shift body weight as far as possible to one side, allowing the elbow on that side to flex. </p>\n<p>Reverse the motion, moving completely over to the other side.</p>\n<p>Return to the starting position, and repeat for the desired number of repetitions.</p>",
        "name": "Isometric Wipers",
        "name_original": "Isometric Wipers",
        "uuid": "20b88059-3958-4184-9134-b48656ad868d",
    },

    {
        "id": 583,
        "description": "<ol>\n<li>Stand with feet together and arms at the sides</li>\n<li>Jump to a position with the legs spread wide and the hands touching overhead</li>\n<li>Repeat</li>\n</ol>",
        "name": "Jumping Jacks",
        "name_original": "Jumping jacks",
        "uuid": "07f9caa2-70c6-46e9-9930-bf90123f217a",
    },


    {
        "id": 249,
        "description": "<p>Hold the kettlebell securely in both hands. Keep your back flat throughout the move, avoiding any rounding of the spine.Keeping your knees \"soft\", hinge your hips backwards, letting the kettlebell swing between your knees.</p>\n<p>You want to bend from the hips as far as you can <em>without letting your back round forwards</em>. Then, snap your hips forwards quickly and standing up straight, locking your body in an upright posture.</p>\n<p>The speed you do this will cause your arms and the kettlebell to swing up in front of you. Don't try to <em>lift</em> the kettlebell with your arms. The snapping forwards of your hips will cause the kettlebell to swing forwards through momentum. Depending on the weight of the kettlebell and the speed of your hip movement, your arms will swing up to about shoulder height. At the top of this swing, let your hips hinge backwards again as the kettlebell swings back down to between your legs and the start of the next repetition.</p>",
        "name": "Kettlebell Swings",
        "name_original": "Kettlebell Swings",
        "uuid": "9d1b45e0-0689-411d-915b-5c5e3452e2b0",
    },

    {
        "id": 546,
        "description": "<p>Placing a barbell in the landmine or in a corner, grasp the head of the barbell besides your body while looking away from the barbell. Twist the barbell as if you were swinging a golf club. Repeat.</p>",
        "name": "Landmine Twists",
        "name_original": "Landmine Twists",
        "uuid": "e5e17f4e-b5a5-4892-bf91-e7a16bd1684f",
    },

    {
        "id": 306,
        "description": "<p>-(1) Perform a lateral raise, pausing at the top of the lift (2).</p>\n<p>-Instead of lowering the weight, bring it to the front of your body so that you appear to be at the top position of a front raise.  You will do this by using a Pec Fly motion, maintaining straight arms. (3)</p>\n<p>-Now lower the weight to your quadriceps, or, in other words, lower the dumbbells as though you are completing a Front Raise repetition. (4)</p>\n<p>-Reverse the motion:  Perform a front raise (5), at the apex of the lift use a Reverse Fly motion to position the weights at the top of a Lateral Raise (6), and finally, lower the weights until your palms are essentially touching the sides of your thighs (7).  THIS IS ONE REP.</p>\n<p>(1) l  <em>front view  </em>(2) -l- <em> FV  </em>  (3) l-  <em>side view</em>   (4) l  <em>SV/FV</em>   (5) l-  <em>SV  </em> (6) -l-  <em>FV  </em>  (7)  l  <em>FV/SV</em></p>",
        "name": "Lateral-to-Front Raises",
        "name_original": "Lateral-to-Front Raises",
        "uuid": "aba7ba7a-6ff0-47d4-ab66-8952e21e38c5",
    },

    {
        "id": 154,
        "description": "<p>Lay on a bench and put your calves behind the leg holder (better if they are hold on around the lower calves). Hold a grip on the bars to make sure the body is firmly in place. Bend your legs bringing the weight up, go slowly back. During the exercise the body should not move, all work is done by the legs.</p>",
        "name": "Leg Curls (laying)",
        "name_original": "Leg curls (laying)",
        "uuid": "cd4fac32-48fb-4237-a263-a44c5108790a",
    },

    {
        "id": 115,
        "description": "<p>The exercise is very similar to the wide leg press:</p>\n<p>Sit on the machine and put your feet on the platform so far apart  that you could just put another foot in between them. The feet are parallel and point up.</p>\n<p>Lower the weight so much, that the knees form a right angle. Push immediately the platform up again, without any pause. When in the lower position, the knees point a bit outwards and the movement should be always fluid.</p>",
        "name": "Leg Presses (narrow)",
        "name_original": "Leg presses (narrow)",
        "uuid": "560e1a20-33e1-45db-ba5b-63f8c51af76d",
    },
    {
        "id": 114,
        "description": "<p>Sit on the machine and put your feet on the platform, a bit more than shoulder wide. The feet are turned outwards by a few degrees.</p>\n<p>Lower the weight so much, that the knees form a right angle. Push immediately the platform up again, without any pause. When in the lower position, the knees point a bit outwards and the movement should be always fluid.</p>",
        "name": "Leg Presses (wide)",
        "name_original": "Leg presses (wide)",
        "uuid": "3caed096-c65e-434d-9792-33db636743e7",
    },

    {
        "id": 125,
        "description": "<p>Lay down on a bench and hold onto the recliner with your hands to keep you stable. Hold your legs straight and lift them till they make an angle of about 45°. Make a short pause of 1 sec. and go slowly down to the initial position. To increase the intensity you can make a longer pause of 7 sec. every 5th time.</p>",
        "name": "Leg Raises, Lying",
        "name_original": "Leg raises, lying",
        "uuid": "7c3cc0ce-e335-4f5c-aa32-1576a908a539",
    },
    {
        "id": 126,
        "description": "<p>Put your forearms on the pads on the leg raise machine, the body is hanging freely. Lift now your legs with a fast movement as high as you can, make a short pause of 1sec at the top, and bring them down again. Make sure that during the exercise your body does not swing, only the legs should move.</p>",
        "name": "Leg Raises, Standing",
        "name_original": "Leg raises, standing",
        "uuid": "3d8273d2-1fbe-4a8f-99a6-d2b5e204cb41",
    },
    {
        "id": 438,
        "description": "<p>Lay down on a bench, the bar should be directly above your eyes, the knees are somewhat angled and the feet are firmly on the floor. Concentrate, breath deeply and grab the bar more than shoulder wide. Bring it slowly down till it briefly touches your chest at the height of your nipples. Push the bar up.</p>\n<p>If you train with a high weight it is advisable to have a <em>spotter</em> that can help you up if you can't lift the weight on your own.</p>\n<p>With the width of the grip you can also control which part of the chest is trained more:</p>\n<ul>\n<li>wide grip: outer chest muscles</li>\n<li>narrow grip: inner chest muscles and triceps</li>\n<li>Don't stretch your arms completly</li>\n</ul>",
        "name": "Limbert",
        "name_original": "Limbert",
        "uuid": "35d51ae3-0549-45aa-9a48-016cb84d4060",
    },
    {
        "id": 143,
        "description": "<p>Sit down, put your feet on the supporting points and grab the bar with a wide grip. Pull the weight with a rapid movement towards your belly button, not upper. Keep your arms and elbows during the movement close to your body. Your shoulders are pulled together. Let the weight slowly down till your arms are completely stretched.</p>",
        "name": "Long-Pulley (low Row)",
        "name_original": "Long-Pulley (low row)",
        "uuid": "b192c4eb-31c6-458e-b566-d3f38b5aa30c",
    },
    {
        "id": 144,
        "description": "<p>The exercise is the same as the regular long pulley, but with a narrow grip:</p>\n<p>Sit down, put your feet on the supporting points and grab the bar with a wide grip. Pull the weight with a rapid movement towards your belly button, not upper. Keep your arms and elbows during the movement close to your body. Your shoulders are pulled together. Let the weight slowly down till your arms are completely stretched.</p>",
        "name": "Long-Pulley, Narrow",
        "name_original": "Long-Pulley, narrow",
        "uuid": "bcabc8cf-c005-4630-8853-ef4922e7fd91",
    },
    {
        "id": 389,
        "description": "<p>Unrack the bar and set your stance wide, beyond your hips.  Push your hips back and sit down to a box that takes you below parallel.  Sit completely down, do not touch and go.  Then explosively stand up.  Stay tight in your upper back and torso throughout the movement.</p>",
        "name": "Low Box Squat - Wide Stance",
        "name_original": "Low Box Squat - Wide Stance",
        "uuid": "222e10a6-e3ba-40fd-9598-6c1d664b450a",
    },

    {
        "id": 456,
        "description": "<p>With the palms of your hands pointed forward set shoulder-width apart, push your body off the ground, locking your elbows as if you’re at the top of a dip exercise. Make sure to keep your shoulders down as you lock your knees and hold your legs together tightly, forming a 90-degree angle with your torso. Your legs should be parallel to the ground. hold for time.</p>",
        "name": "L-sit",
        "name_original": "L-sit",
        "uuid": "127351c9-0565-4284-8f43-412e0caf13ba",
    },

    {
        "id": 656,
        "description": "<p>Challenging your balance is an essential part of a well-rounded exercise routine. Lunges do just that, promoting functional movement, while also increasing strength in your legs and glutes.</p>",
        "name": "Lunges",
        "name_original": "Lunges",
        "uuid": "06cae49f-3db9-4904-880f-8a6cc1897ed2",
    },
    {
        "id": 533,
        "description": "<p>Lay down and pull a dumbbell over your head.</p>",
        "name": "Lying Triceps Extension",
        "name_original": "Lying Triceps Extension",
        "uuid": "a92aee3f-5d8b-4bed-bdfc-562034ae31ba",
    }, {
        "id": 582,
        "description": "<p>Lie down on back on mat, stretch out arms for support. Raise legs, keeping them stretched. Drop both legs to right side, then back up. Drop down to left side then back up. Repeat.</p>",
        "name": "Lying Windscreen Wipers",
        "name_original": "Lying windscreen wipers",
        "uuid": "954f3179-4a47-438a-a162-3f1147d60ad8",
    },
    {
        "id": 629,
        "description": "<p>From the floor do a back low row than a burpee into shoulder press and back down into pushup position</p>",
        "name": "Man Breakers",
        "name_original": "Man Breakers",
        "uuid": "f19577a0-d5a1-47a5-a3fd-32f5744123be",
    }, {
        "id": 534,
        "description": "Grab dumbbells and extend arms to side and hold as long",
        "name": "Metin",
        "name_original": "Metin",
        "uuid": "ba3273b0-d782-4e9e-bdf4-8003d6b7428f",
    },
    {
        "id": 229,
        "description": "<p>The military press is a variation of the overhead press weight training exercise using very strict form and no pre-movement momentum.The military press targets the deltoid muscles in the shoulders as well as the triceps. Additionally, it works the core and legs, which the lifter uses to help stabilize the weight.The lift begins with the lifter standing and the barbell on the anterior deltoids. The lifter then raises the barbell overhead by pressing the palms of the hands against the underside of the barbell.</p>",
        "name": "Military Press",
        "name_original": "Military Press",
        "uuid": "89e6d2ea-9a17-4a77-9a52-d5bcf39d57fd",

    }, {
        "id": 256,
        "description": "<p>On an SZ-bar grip your hands on the outside of each bend and stand with your arms straight down, palms facing your legs. Pull the bar (bending your arms at the elbow) to your chest, and the push the bar above your head (arms as straight as possible). Return the bar to your chest by dropping your arms at the elbows. Return the bar to it's origional position (stand with your arms straight down, palms facing your legs.)</p>",
        "name": "Military Press",
        "name_original": "Military Press",
        "uuid": "8aadcd68-f085-44a0-a897-8b6ecf827ed0",

    }, {
        "id": 626,
        "description": "<p>The body is then explosively pulled up by the arms in a radial pull-up, with greater speed than a regular pull-up. When the bar approaches the upper chest, the wrists are swiftly flexed to bring the forearms above the bar. The body is leaned forward, and the elbows are straightened by activating the triceps. The routine is considered complete when the bar is at the level of the waist and the arms are fully straight.</p>\n<p>To dismount, the arms are bent at the elbow, and the body is lowered to the floor, and the exercise can be repeated.</p>\n<p>As a relatively advanced exercise, muscle-ups are typically first learned with an assistive kip. The legs swing (kip) up and provide momentum to assist in the explosive upward force needed to ascend above the bar. More advanced athletes can perform a strict variation of the muscle-up which is done slowly, without any kip. This variation begins with a still dead hang and uses isometric muscle contraction to ascend above the bar in a slow, controlled fashion.</p>",
        "name": "Muscle up",
        "name_original": "Muscle up",
        "uuid": "85b2f07b-2ce3-4c85-82ab-474a6e5620b2",

    }, {
        "id": 93,
        "description": "<p>Sit yourself on the decline bench and fix your legs. Cross your arms over the chest and bring with a rolling movement your upper body up, go now without a pause and with a slow movement down again. Don't let your head move during the exercise.</p>",
        "name": "Negative Crunches",
        "name_original": "Negative crunches",
        "uuid": "4bd55b0a-559a-4458-aabd-e66619b63610",

    }, {
        "id": 556,
        "description": "<ol>\n<li>The standing oblique cable crunch hits the abs and obliques. Set up the high pulley cable machine by attaching a grip attachment and selecting the weight you want to use.</li>\n<li>Grasp the handle with your left hand and face your body at a right angle to the cable pulley with your feet firmly on the floor and shoulder width apart.</li>\n<li>Your forearm should be at a right angle to your upper arm.</li>\n<li>Pull down with your left oblique as far as possible.</li>\n<li>Pause, then lower the weight back down.</li>\n<li>Don't let the weight drop to the stack at any time throughout the exercise.</li>\n<li>Repeat this movement for the desired number of reps and then switch to your other side. </li>\n</ol>",
        "name": "Oblique Cable Crunch",
        "name_original": "Oblique Cable Crunch",
        "uuid": "0e16851c-9b0b-4c0d-9e52-58499929291f",

    },
    {
        "id": 197,
        "description": "<p>Hands at shoulder height, curl arms in toward head, then back out.</p>",
        "name": "Overhand Cable Curl",
        "name_original": "Overhand Cable Curl",
        "uuid": "e7fdd8fb-5e07-4a29-9d56-a005338b2e19",

    }, {
        "id": 355,
        "description": "<p>The barbell is held overhead in a wide-arm snatch grip; however, it is also possible to use a closer grip if balance allows.</p>",
        "name": "Overhead Squat",
        "name_original": "Overhead Squat",
        "uuid": "b9c97cec-ffc7-4e9a-8ecc-08b285aed3b4",

    }, {
        "id": 455,
        "description": "<p>In this version of the squat you hold the bar overhead as you perform the exercise.</p>\nSteps\n<ol>\n<li>Stand with your feet a bit wider than shoulder width apart with your toes pointed slightly outward.</li>\n<li>Grasp a barbell using a wide side snatch grip with your arms and elbows fully extended.</li>\n<li>Keeping the bar overhead, bend your knees and lower your body until your thighs are parallel with the floor.</li>\n<li>Return to starting position</li>\n</ol>",
        "name": "Overhead Squat With Barbell",
        "name_original": "Overhead Squat with Barbell",
        "uuid": "399f50bc-dbb8-4fd4-ab3e-d6b2ad34a8a6",


    }, {
        "id": 202,
        "description": "<p>Back excercise with a barbell with a starting position which is in a bent over position with the back paralell to the ground. The barbell is on the ground at chest level.For the movement grab the barbell at shoulder width grip and pull towards your chest without losing the bent over position and without moving anything but your arms</p>",
        "name": "Pendelay Rows",
        "name_original": "Pendelay rows",
        "uuid": "eed05679-d1cb-44a2-a17d-dd5b0097c874",

    }, {
        "id": 160,
        "description": "<p>One legged squat</p>",
        "name": "Pistol Squat",
        "name_original": "Pistol Squat",
        "uuid": "b1d6d536-7f4a-4dd3-8b76-62d7a984e115",
    }, {
        "id": 540,
        "description": "The plank is one of the most common core exercises, ever, but most guys get it wrong. Creating tension in your abs primes your body to maximally contract all of its muscles. Get into a position on the floor supporting your weight on your forearms and toes. Arms are bent and directly below the shoulder. </p>\n<p>Keep your body straight at all times and hold this position as long as possible. To increase difficulty an arm or leg can be raised while performing this exercise.",
        "name": "Plank",
        "name_original": "Plank",
        "uuid": "b1476958-08fb-4a3f-976e-a10b7cf07a82",

    }, {
        "id": 193,
        "description": "<p> Place the EZ curl bar on the rest handles in front of the preacher bench. Lean over the bench and grab the EZ curl bar with palms up. Sit down on the preacher bench seat so your upper arms rest on top of the pad and your chest is pressed against the pad. Lower the weight until your elbows are extended and arms are straight. Bring the weights back up to the starting point by contracting biceps. Repeat</p>",
        "name": "Preacher Curls",
        "name_original": "Preacher Curls",
        "uuid": "84b4e711-8b29-4541-ad58-dad967f82390",

    },
    {
        "id": 429,
        "description": "<p>Lying on stomach with head on towel.</p>\n<p>Stretch arms straight out to your sides.</p>\n<p>Slowly lift your arms, pulling your shoulderblades together, hold for 3 seconds.</p>\n<p> </p>",
        "name": "Prone Scapular Retraction - Arms at Side",
        "name_original": "Prone Scapular Retraction - Arms at side",
        "uuid": "82a0a7e2-2d4d-4336-9412-f9878575f709",

    }, {
        "id": 441,
        "description": "<ul>\n<li>Stand up, take the pull bar and pull down with extended arms.</li>\n<li>Going back to the original position</li>\n</ul>",
        "name": "Pull-over  (Extended Arms)",
        "name_original": "Pull-over  (Extended Arms)",
        "uuid": "e1d2b072-2791-4a63-a41e-1c43914a4886",


    }, {
        "id": 107,
        "description": "<p>Grab the pull up bar with a wide grip, the body is hanging freely. Keep your chest out and pull yourself up till your chin reaches the bar or it touches your neck, if you want to pull behind you. Go with a slow and controlled movement down, always keeping the chest out.</p>",
        "name": "Pull-ups",
        "name_original": "Pull-ups",
        "uuid": "7ce6b090-5099-4cd0-83ae-1a02725c868b",

    }, {
        "id": 195,
        "description": "<p>Start with your body streched, your hands are shoulder-wide appart on the ground. Push yourself off the ground till you strech your arms. The back is always straight and as well as the neck (always look to the ground). Lower yourself to the initial position and repeat.</p>",
        "name": "Push Ups",
        "name_original": "Push ups",
        "uuid": "9d756e84-6b77-4f17-b21d-7d266d6a8bd2",

    }, {
        "id": 161,
        "description": "<p>Deadlift to be done using a Smith machine or a free rack. Bar or barbell hould be just right under the knee cap level. Lift using the glutes and through the heels, then come back to starting postion with a control movement of 2 seconds.</p>\n<p>This exercise targets mainly the lower back and glutes.</p>",
        "name": "Rack Deadlift",
        "name_original": "Rack deadlift",
        "uuid": "2738e6e0-2158-4a7e-8fbd-32657d043a65",

    }, {
        "id": 237,
        "description": "<p>Seated on a bench bWith the dumbbells on the floor bend over at 45 Degrees and then slowly raise each dumbbell to shoulder height and hold for a couple seconds before lowering to the starting position. </p>\n<p> </p>",
        "name": "Rear Delt Raises",
        "name_original": "Rear Delt Raises",
        "uuid": "69f271a6-5408-49d9-8a3a-1f6088835f30",

    }, {
        "id": 460,
        "description": "<p><strong>Preparation</strong></p>\n<p> </p>\n\nStand facing rope attachment on high pulley cable. Grasp each end of rope just above enlarged ends. Step back with one foot so arms and shoulders are positioned straight forward with cable taut. Point elbows outward.\n\n<p> </p>\n<p><strong>Execution</strong></p>\n<p> </p>\n\nPull rope to upper chest or neck, keeping elbows at shoulder height until elbows travel slightly behind back. Keep upper arms perpendicular to trunk. Return until arms are extended forward. Repeat.\n\n<p>Target</p>\n<p>             Posterior Deltoid</p>",
        "name": "Rear Delt Rope Pull",
        "name_original": "Rear Delt Rope Pull",
        "uuid": "251ef1f0-0a96-4b7b-80f9-5af949fbe112",

    }, {
        "id": 670,
        "description": "<p>Get into pushup position gripping some dumbbells. Perform one pushup, then drive your left elbo up, bringing the dumbell up to your body. Return the dumbell to starting position. </p>\n<p>Perform another pushup and then row with the other arm to complete one rep.</p>",
        "name": "Renegade Row",
        "name_original": "Renegade Row",
        "uuid": "f8a14f95-bdf7-433a-a1db-ef26b4ecd2e9",


    },
    {
        "id": 555,
        "description": "<p>1. Lie on your back with your knees together and your legs bent to 90 degrees, feet planted on the floor. Place your palms face down on the floor for support.</p>\n<p>2.Tighten your abs to lift your hips off the floor as you crunch your knees inward to your chest. Pause at the top for a moment, then lower back down without allowing your lower back to arch and lose contact with the floor.</p>",
        "name": "Reverse Crunch",
        "name_original": "Reverse Crunch",
        "uuid": "0504d15b-e243-4070-96d9-06307682703e",


    }, {
        "id": 399,
        "description": "<p>Upper chest focuses exercise that also works triceps</p>",
        "name": "Reverse Grip Bench Press",
        "name_original": "Reverse Grip Bench Press",
        "uuid": "30a8ef85-a24c-4f44-b558-55d3279b29c5",

    }, {
        "id": 409,
        "description": "<p>Plank with stomach towards ceiling</p>",
        "name": "Reverse Plank",
        "name_original": "Reverse Plank",
        "uuid": "75ec610d-216a-49fe-b395-c5fd8ee14b53",

    }, {
        "id": 360,
        "description": "<p>Dips peformed on gymnastic rings.</p>",
        "name": "Ring Dips",
        "name_original": "Ring Dips",
        "uuid": "f7b6a6b0-6dd8-4648-9a32-83039f5e28e1",

    }, {
        "id": 579,
        "description": "<p>Start by holding the rings with a supine grip at chest level. The elbows are held behind the back, applying inward force.</p>\n<p>The eccentric phase begins by extending the arms as slowly as possible, keeping the whole body tight and trying to oversupine the forearms.</p>\n<p>The concentric phase is executed by flexing the arms to return to the starting position, trying to keep the elbows static throughout the movement.</p>\n<p>The difficulty can be increased by placing the rings at a lower height.</p>",
        "name": "Ring Pelican Curl",
        "name_original": "Ring pelican curl",
        "uuid": "c7e2830c-69e1-407e-897e-1e7455870549",

    }, {
        "id": 446,
        "description": "<p>Use Hyperextension chair.  Sit on chair, arms crossed on over your chest. then situp slowly and back</p>",
        "name": "Roman Chair Crunch",
        "name_original": "Roman Chair Crunch",
        "uuid": "b325a755-1cb3-4e6b-8501-d2607d91aab3",

    }, {
        "id": 477,
        "description": "<p>To use a rowing machine</p>\n<p>1. Wrap fingers lightly around the handle and keep wrists straight.</p>\n<p>2. extend arms in front of you, keeping shoulders relaxed—no hunching!</p>\n<p>3. hinge forward from the hips and bend knees until they’re over ankles </p>\n<p>4. push through feet, extend legs, and lean back slightly; keep shoulders relaxed.</p>\n<p>5. draw elbows straight back to sides until hands reach ribs.</p>\n<p>6. to start the next stroke, extend arms, and then bend knees to slide the seat forward.</p>",
        "name": "Rowing",
        "name_original": "Rowing",
        "uuid": "83538ecf-c907-4213-821f-8cd7ea7ff221",

    }, {
        "id": 106,
        "description": "<p>The execution of this exercise is very similar to the regular bent over rowing, only that the bar is fixed here.</p>\n<p>Grab the barbell with a wide grip (slightly more than shoulder wide) and lean forward. Your upper body is not quite parallel to the floor, but forms a slight angle. The chest's out during the whole exercise. Pull now the barbell with a fast movement towards your belly button, not further up. Go slowly down to the initial position. Don't swing with your body and keep your arms next to your body.</p>",
        "name": "Rowing, T-bar",
        "name_original": "Rowing, T-bar",
        "uuid": "3c3f23b5-b523-4a69-8c79-1106a4ce9b6d",

    },

    {
        "id": 427,
        "description": "<ol>\n<li>Lie down on the floor placing your feet either under something that will not move or by having a partner hold them. Your legs should be bent at the knees.</li>\n<li>Elevate your upper body so that it creates an imaginary V-shape with your thighs. Your arms should be fully extended in front of you perpendicular to your torso and with the hands clasped. This is the starting position.</li>\n<li>Twist your torso to the right side until your arms are parallel with the floor while breathing out.</li>\n<li>Hold the contraction for a second and move back to the starting position while breathing out. Now move to the opposite side performing the same techniques you applied to the right side.</li>\n<li>Repeat for the recommended amount of repetitions.</li>\n</ol>",
        "name": "Russian Twist",
        "name_original": "Russian Twist",
        "uuid": "3eb06d8e-fabe-4d4b-8119-b83cc1dadb7c",

    }, {
        "id": 631,
        "description": "<p>Scissors is an abdominal exercise that strengthens the transverse abdominals, helping flatten your belly and strengthen your entire core. Scissors is not only a core strength move, but it is also a great stretch for your hamstrings and your lower back. Everyone is looking for new ways to work the core, to flatten the belly and to improve flexibility. If you learn how to do Scissors you will get everything rolled together in one move.</p>",
        "name": "Scissors",
        "name_original": "Scissors",
        "uuid": "d89d34e8-2bd6-4d85-acf7-cae5d776c5e9",
    },
    {
        "id": 569,
        "description": "<p>jump from seated position onto box</p>",
        "name": "Seated Box Jump",
        "name_original": "seated box jump",
        "uuid": "f4a993f4-c631-4172-9024-e84b5e9732df",

    }, {
        "id": 377,
        "description": "Preparation\nSit on seat and lift one leg onto padded lever. Lean back onto back pad. Grasp handles attatched to leg levers. Push handles together and raise other leg onto other padded lever. Release leg lever handles and grasp other handles to each side.\n\nExecution\nMove legs together until leg levers make contact. Allow legs to seperate far apart until slight stretch and repeat.",
        "name": "Seated Hip Adduction",
        "name_original": "Seated Hip Adduction",
        "uuid": "4b3025a9-b717-4577-9e92-b1c200fe5f1c",

    }, {
        "id": 386,
        "description": "<p>Sit down on a back (better with back support). Take a dumbbell firmly with both hands and hold it with extended arms over your head. With your palms facing upward and holding the weight of the dumbbell, slowly lower the weight behind your head.</p>\n<p> </p>",
        "name": "Seated Triceps Press",
        "name_original": "Seated Triceps Press",
        "uuid": "76964898-192c-4e93-b24c-914ea542002b",

    }, {
        "id": 465,
        "description": "<p>put your feet on a high level and pull your self</p>",
        "name": "Seatups",
        "name_original": "Seatups",
        "uuid": "5d373fe0-32ec-4e5d-9ffa-8a588913b7bc",

    }, {
        "id": 339,
        "description": "<ol>\n<li>Attach a single handle to a low cable.</li>\n<li>After selecting the correct weight, stand a couple feet back with a wide-split stance. Your arm should be extended and your shoulder forward. This will be your starting position.</li>\n<li>Perform the movement by retracting the shoulder and flexing the elbow. As you pull, supinate the wrist, turning the palm upward as you go.</li>\n<li>After a brief pause, return to the starting position.</li>\n</ol>",
        "name": "Shotgun Row",
        "name_original": "Shotgun Row",
        "uuid": "b6492677-07df-4399-bc90-20b58e4dba35",

    },
    {
        "id": 547,
        "description": "<p>Hold a bar or broomstick on either end. Raise the bar above the head and slowly lower the bar behind you to rest on the hips/back/butt. This exercise is to improve pasture and mobility of the shoulders. </p>",
        "name": "Shoulder Dislocations",
        "name_original": "Shoulder dislocations",
        "uuid": "9bb294b3-2bbe-494b-ac9d-c27b95f5b02f",

    }, {
        "id": 119,
        "description": "<p>Sit on a bench, the back rest should be almost vertical. Take a barbell with a shoulder wide grip and bring it up to chest height. Press the weight up, but don't stretch the arms completely. Go slowly down and repeat.</p>",
        "name": "Shoulder Press, Barbell",
        "name_original": "Shoulder press, barbell",
        "uuid": "9926e18f-4e2b-4c20-9477-9bfb08d229bc",

    }, {
        "id": 123,
        "description": "<p>Sit on a bench, the back rest should be almost vertical. Take two dumbbells and bring them up to shoulder height, the palms and the elbows point during the whole exercise to the front. Press the weights up, at the highest point they come very near but don't touch. Go slowly down and repeat.</p>",
        "name": "Shoulder Press, Dumbbells",
        "name_original": "Shoulder press, dumbbells",
        "uuid": "1df6a1b5-7bd2-402f-9d5e-94f9ed6d8b54",

    }, {
        "id": 155,
        "description": "<p>The exercise is basically the same as with a free barbell:</p>\n<p>Sit on a bench, the back rest should be almost vertical. Take a bar with a shoulder wide grip and bring it down to chest height. Press the weight up, but don't stretch the arms completely. Go slowly down and repeat.</p>",
        "name": "Shoulder Press, on Multi Press",
        "name_original": "Shoulder press, on multi press",
        "uuid": "68d505ec-5416-4153-b228-eb1545d5c89f",

    }, {
        "id": 150,
        "description": "<p>Take a barbell and stand with a straight body, the arms are hanging freely in front of you. Lift from this position the shoulders as high as you can, but don't bend the arms during the movement. On the highest point, make a short pause of 1 or 2 seconds before returning slowly to the initial position.</p>\n<p>When training with a higher weight, make sure that you still do the whole movement!</p>",
        "name": "Shrugs, Barbells",
        "name_original": "Shrugs, barbells",
        "uuid": "3721be0c-2a59-4bb9-90d3-695faaf028af",

    }, {
        "id": 151,
        "description": "<p>Stand with straight body, the hands are hanging freely on the side and hold each a dumbbell. Lift from this position the shoulders as high as you can, but don't bend the arms during the movement. On the highest point, make a short pause of 1 or 2 seconds before returning slowly to the initial position.</p>\n<p>When training with a higher weight, make sure that you still do the whole movement!</p>",
        "name": "Shrugs, Dumbbells",
        "name_original": "Shrugs, dumbbells",
        "uuid": "ee61aef0-f0c7-4a7a-882a-3b39312dfffd",

    }, {
        "id": 176,
        "description": "<p>Hold weight in one hand. Bend side ways to the knee. Pull upo to upright position using your obliquus.</p>",
        "name": "Side Crunch",
        "name_original": "Side Crunch",
        "uuid": "53a9f95e-914c-4790-940f-f5346159ea72",
    }, {
        "id": 422,
        "description": "<p>With a weight in one hand, lie on your side opposite the weight. Keep your knees slightly bent. Hold your elbow against your side, and extend your upper arm straight ahead of you. While continuing to hold your elbow against your side, rotate your upper arm 90 degrees upwards.</p>\n<p>It is helpful to place a towel under your armpit to help with the form on this exercise. Placing a support under your head for the duration of the exercise is also a good idea.</p>",
        "name": "Side-lying External Rotation",
        "name_original": "Side-lying External Rotation",
        "uuid": "9f705970-cba3-4cef-a582-c2cc69ae9b5f",
    }, {
        "id": 325,
        "description": "<p>Works your obliques and helps stabilize your spine. Lie on your side and support your body between your forearm and knee to your feet.</p>",
        "name": "Side Plank",
        "name_original": "Side Plank",
        "uuid": "a36f852f-a29f-4f93-81b6-1012047ac1ee",
    }, {
        "id": 319,
        "description": "<p>Stand up or sit , keep both weights in front against legs or at side. Keep arms at around a 90 degree angle. Lift elbows up slowly and squeese traps when at topmost position. Lower the weights slowly back to starting position.</p>\n<p>2 seconds up, 2 seconds down</p>",
        "name": "Side Raise",
        "name_original": "Side raise",
        "uuid": "bd892f71-2b19-41c4-90e2-2cd4df82c621",
    }, {
        "id": 302,
        "description": "<p>-start in push up position</p>\n<p>-lean the body weight to the right side, and complete a push up with the chest over the right hand</p>\n<p>-come back to the centered position</p>\n<p>-on rep 2, lean to the left side</p>",
        "name": "Side to Side Push Ups",
        "name_original": "Side to Side Push Ups",
        "uuid": "fc63159a-3939-426c-b236-d4b39adbf28c",
    },
    {
        "id": 205,
        "description": "<p>Sit on the preacher curl bench and perform a bicep curl with a dumbbell in one hand. Your other hand can be at rest, or beneath your curling arm's elbow.</p>",
        "name": "Single-arm Preacher Curl",
        "name_original": "Single-arm preacher curl",
        "uuid": "83e079a9-b61c-413b-b055-f9cf515904f1",

    }, {
        "id": 588,
        "description": "<ol>\n<li>Standing in front of a cable machine, hold the cable attachment in one hand at shoulder height.</li>\n<li>Keep your ribs down, abs tight and tailbone tucked between your knees.</li>\n<li>Pull the cable back, finishing with your shoulder blade locked inward and down.</li>\n<li>Prevent your shoulder from shifting forward at the finish of the movement.</li>\n</ol>",
        "name": "Single-arm, Standing Split-stance Cable Row",
        "name_original": "single-arm, standing split-stance cable row",
        "uuid": "6de29bfa-4b0e-4da3-9593-925cc8d49718",

    }, {
        "id": 627,
        "description": "<ol>\n<li>Hold a kettlebell in one hand, hanging to the side. Stand on one leg, on the same side that you hold the kettlebell.</li>\n<li>Keeping that knee slightly bent, perform a stiff-legged deadlift by bending at the hip, extending your free leg behind you for balance. Continue lowering the kettlebell until you are parallel to the ground, and then return to the upright position. Repeat for the desired number of repetitions.</li>\n</ol>",
        "name": "Single Leg Deadlift",
        "name_original": "Single Leg Deadlift",
        "uuid": "9b8d2089-4d62-436e-a14f-167c6425f72d",

    }, {
        "id": 648,
        "description": "<ol>\n<li>Lay on the floor with your feet flat and knees bent.</li>\n<li>Raise one leg off of the ground, pulling the knee to your chest. This will be your starting position.</li>\n<li>Execute the movement by driving through the heel, extending your hip upward and raising your glutes off of the ground.</li>\n<li>Extend as far as possible, pause and then return to the starting position.</li>\n</ol>",
        "name": "Single-leg Glute Bridge",
        "name_original": "Single-leg Glute Bridge",
        "uuid": "a2c1fbdf-a46e-42a6-8a63-31cee54d545d",

    }, {
        "id": 586,
        "description": "<p>Sit and pull v grip to chest, return to starting position then repeate. </p>",
        "name": "Sit Pull V",
        "name_original": "Sit Pull V",
        "uuid": "187b5b75-b69a-49d1-a000-05069d2fa439",

    }, {
        "id": 103,
        "description": "<p>Sit on a bench for calf raises and check that the feet are half free and that you can completely stretch the calf muscles down. Pull your calves up, going as far (up) as you can. Make at the highest point a short pause of 1 or 2 seconds and go down.</p>",
        "name": "Sitting Calf Raises",
        "name_original": "Sitting calf raises",
        "uuid": "399666e7-30a7-4b86-833d-4422e4c72b61",

    }, {
        "id": 95,
        "description": "<p>Sit on a mat, your calves are resting on a bench, the knees make a right angle. Hold your hands behind your neck. Go now up with a rolling movement of your back, you should feel how the individual vertebrae lose contact with the mat. At the highest point, contract your abs as much as you can and hold there for 2 sec. Go now down, unrolling your back.</p>\n<p> </p>",
        "name": "Sit-ups",
        "name_original": "Sit-ups",
        "uuid": "fb750082-7034-4c51-b1e4-dfa0fe2dac8e",
    },
    {
        "id": 411,
        "description": "<p>Do a single, double footed jump for each swing of the rope.</p>\n<p>Work on a smooth, rhythmical movement, bouncing lightly on the balls of your feet.</p>",
        "name": "Skipping - Standard",
        "name_original": "Skipping - Standard",
        "uuid": "2c864448-288d-4e79-9da6-a3c99af3c1fb",
        
    },
    {
        "id": 612,
        "description": "<p>From a basic plank place your feet on a towel or old tee shirt and rapidly slide your feet up towards your chest. Kick it up a knotch by sliding up a 45 degree angle.</p>",
        "name": "Slide Planks",
        "name_original": "Slide planks",
        "uuid": "d0e77084-29e5-4fb4-b573-01ad34ccfb68",
    },
    {
        "id": 621,
        "description": "<p>Suspend both legs in the sling. Begin with your legs straight and your hips extended. This will be your starting position.</p>\n<p>\"Walk\" forward on your arms or elbows. The further forward you go the harder it will become.</p>\n<p>Pull your knees up to chest quickly and release slowly back to the starting position.</p>",
        "name": "Sling: Suspended Crunch",
        "name_original": "Sling: Suspended crunch",
        "uuid": "8b5e212b-e96f-4832-90fc-693a44ede9d9",
       
    },
    {
        "id": 218,
        "description": "<p>Perform a standard bench press on the smith machine, but have your hands on the bar about shoulder width apart, and keep your elbows close to your body.</p>",
        "name": "Smith Machine Close-grip Bench Press",
        "name_original": "Smith machine close-grip bench press",
        "uuid": "9d1c13b0-2e9b-4585-b1d7-fc48ba4c692f",
       
    },
    {
        "id": 271,
        "description": "<p>Stand with your feet at hip width and your shins against the bar. Grasp the bar at double shoulder width and, keeping your lower back flat, drive your heels into the floor to begin lifting the bar. When it's above your knees, explosively extend your hips and shrug your shoulders. Let the momentum carry the weight overhead.</p>",
        "name": "Snach",
        "name_original": "Snach",
        "uuid": "3490d1c7-c9ce-4a08-acd3-e0bdada71c37",
        
    },
    
    
    {
        "id": 170,
        "description": "<p>Lie on your back with your legs straight and arms at your sides, keeping your elbows bent at 90 degrees. As you sit up, twist your upper body to the left and bring your left knee toward your right elbow while you swing your left arm back. Lower your body to the starting position, and repeat to your right. That's 1 rep.</p>",
        "name": "Splinter Sit-ups",
        "name_original": "Splinter Sit-ups",
        "uuid": "ec706f20-6ea7-40f9-96f4-994c66554e17",
        
    },
    {
        "id": 474,
        "description": "<p>High-bar squats with a barbell on your back.</p>",
        "name": "Squat",
        "name_original": "Squat",
        "uuid": "9bf001d2-c174-4d59-beb1-e6d96e3b003f",
       
    },
   
    {
        "id": 111,
        "description": "<p>Make sure you have put the barbell at a height where you can comfortably take it out and put it back in. Take it out and make yourself ready:</p>\n<ul>\n<li>The bar is somewhat lower than your shoulders</li>\n<li>The feet are quite apart and point out</li>\n<li>The head is in your neck and looks up</li>\n<li>The chest is out</li>\n</ul>\n<p>Go now slowly down, till your thighs are parallel with the floor, not lower. The knees point outwards, your butt, out. Make a small pause of 1 second and with as much energy as you can, push the weight up. Make a pause of 2 seconds and repeat.</p>",
        "name": "Squats",
        "name_original": "Squats",
        "uuid": "c4856da3-8454-4857-8997-336d06df590f",
       
    },
    {
        "id": 102,
        "description": "<p>Get onto the calf raises machine, you should able to completely push your calves down. Stand straight, don't make a hollow back and don't bend your legs. Pull yourself up as high as you can. Make a small pause of 1 - 2 seconds and go slowly down.</p>",
        "name": "Standing Calf Raises",
        "name_original": "Standing calf raises",
        "uuid": "abc4c62e-27b2-4c31-8766-40f8dca84cab",
    },
    {
        "id": 382,
        "description": "<p>Grab a wrist roller tool with both hands while standing with your feet about shoulder width apart. If your gym does not have a wrist roller tool, you can easily put one together. All you need is a 5 or 10 pound weight plate, a strong thin rope about 3 feet long and a 6-8 inch stick or bar. Securely fasten the rope to the middle of the bar/stick and tie the other end of the rope to the weight plate. To begin this exercise, grab the bar/stick with both hands using an overhand grip. Extend both arms straight out in front of you, parallel to the floor. Next, roll the weight up from the floor by rapidly twisting the bar/stick with your hands and wrists. Once the weight reaches the top, slowly lower the plate back to the floor by reversing the motion of your hands and wrists. Repeat (if you can!).</p>",
        "name": "Standing Rope Forearm",
        "name_original": "Standing Rope Forearm",
        "uuid": "22eb7cf6-bf6c-42c3-a1c8-85c2fc2ed931",
    },
   
    {
        "id": 216,
        "description": "<p>Use the straight bar attachment on a high pulley. Grasp the two ends of the bar with your palms facing downward and your arms straight out in front of you. Pull your hands down towards your hips, while keeping your arms straight, then raise them back up to the starting position.</p>",
        "name": "Straight-arm Pull Down (bar Attachment)",
        "name_original": "Straight-arm pull down (bar attachment)",
        "uuid": "034a4dcd-d085-43fa-a2b5-6d54b444ac20",
        
    },
    {
        "id": 215,
        "description": "<p>Use the rope attachment on a high pulley. Grasp the two ends of the rope with your arms straight out in front of you. Pull your hands down towards your hips, while keeping your arms straight, then raise them back up to the starting position.</p>",
        "name": "Straight-arm Pull Down (rope Attachment)",
        "name_original": "Straight-arm pull down (rope attachment)",
        "uuid": "dc53ad5b-bd04-46d0-9499-28e9965f0e6d",
        
    },
    {
        "id": 557,
        "description": "<ol>\n<li>Begin with a bar loaded on the ground. Approach the bar so that the bar intersects the middle of the feet. The feet should be set very wide, near the collars. Bend at the hips to grip the bar. The arms should be directly below the shoulders, inside the legs, and you can use a pronated grip, a mixed grip, or hook grip. Relax the shoulders, which in effect lengthens your arms.</li>\n<li>Take a breath, and then lower your hips, looking forward with your head with your chest up. Drive through the floor, spreading your feet apart, with your weight on the back half of your feet. Extend through the hips and knees.</li>\n<li>As the bar passes through the knees, lean back and drive the hips into the bar, pulling your shoulder blades together.</li>\n<li>Return the weight to the ground by bending at the hips and controlling the weight on the way down.</li>\n</ol>",
        "name": "Sumo Deadlift",
        "name_original": "sumo deadlift",
        "uuid": "73b4ef1a-1a3b-4328-b455-fcd27a100c0a",
        
    },
    
    {
        "id": 570,
        "description": "<p>Stand with your feet wider than your shoulders, with your toes pointed out at a 45 degree angle and barbell on your shoulder.</p>\n<p>While keeping your back straight, descend slowly by bending at the knees and hips as if you are sitting down (squatting).</p>\n<p>Lower yourself until your quadriceps and hamstrings are parallel to the floor.</p>\n<p>Return to the starting position by pressing upwards and extending your legs while maintaining an equal distribution of weight on your forefoot and heel.</p>",
        "name": "Sumo Squats",
        "name_original": "Sumo Squats",
        "uuid": "f4dd363c-b49c-419a-b8ae-0d8e18728d8e",
        
    },
    
    {
        "id": 330,
        "description": "<p>Lay flat on your stomach with your arms extended in front of you on the ground as your legs are lying flat. Lift both your arms and legs at the same time, as if you were flying, and contract the lower back. Make sure that you are breathing and, depending on your fitness level, hold the movement for at least two to five seconds per repetition.</p>",
        "name": "Superman",
        "name_original": "Superman",
        "uuid": "6572a8e9-083c-4622-8f0c-6107a013a490",
    },
    
    {
        "id": 279,
        "description": "<ol>\n<li>Start with a dumbbell in each hand and your palms facing your torso. Keep your back straight with a slight bend in the knees and bend forward at the waist. Your torso should be almost parallel to the floor. Make sure to keep your head up. Your upper arms should be close to your torso and parallel to the floor. Your forearms should be pointed towards the floor as you hold the weights. There should be a 90-degree angle formed between your forearm and upper arm. This is your starting position.</li>\n<li>Now, while keeping your upper arms stationary, exhale and use your triceps to lift the weights until the arm is fully extended. Focus on moving the forearm.</li>\n<li>After a brief pause at the top contraction, inhale and slowly lower the dumbbells back down to the starting position.</li>\n<li>Repeat the movement for the prescribed amount of repetitions.</li>\n</ol>\n<p><strong>Variations:</strong> This exercise can be executed also one arm at a time much like the one arm rows are performed.</p>\n<p>Also, if you like the one arm variety, you can use a low pulley handle instead of a dumbbell for better peak contraction. In this case, the palms should be facing up (supinated grip) as opposed to the torso (neutral grip).</p>",
        "name": "Tricep Dumbbell Kickback",
        "name_original": "Tricep Dumbbell Kickback",
        "uuid": "88568115-5e88-4afc-b005-e2f7d453ac13",
      },
    {
        "id": 375,
        "description": "<p>Tricep Press down machine with free weights in free weight section</p>",
        "name": "Tricep Push Down FreeWieghts",
        "name_original": "Tricep Push Down FreeWieghts",
        "uuid": "ae7d5a85-c76d-4016-b1d2-a9f9e666f9ea",
    },
    {
        "id": 162,
        "description": "<p>lift on parallel bars hold for 1 second, and lower slowly and control for 4 seconds, then come back with no rest (tempo: 4010)</p>",
        "name": "Triceps Dips",
        "name_original": "Triceps dips",
        "uuid": "5a29197f-e159-4a98-9b40-534c41ab34a1",
    
    },
    {
        "id": 89,
        "description": "<p>Grab the cable, stand with your feet shoulder wide, keep your back straight and lean forward a little. Push the bar down, making sure the elbows don't move during the exercise. Rotate your hands outwards at the very end and go back to the initial position without pause.</p>",
        "name": "Triceps Extensions on Cable",
        "name_original": "Triceps extensions on cable",
        "uuid": "f1b5e525-6232-4d60-a243-9b2cd6c55298",
    
    },
    {
        "id": 90,
        "description": "<p>Grab the bar, stand with your feet shoulder wide, keep your back straight and lean forward a little. Push the bar down, making sure the elbows don't move during the exercise. Without pause go back to the initial position.</p>",
        "name": "Triceps Extensions on Cable With Bar",
        "name_original": "Triceps extensions on cable with bar",
        "uuid": "20a76bd0-1e56-4a4e-bd79-0ab118552bde",
    
    },
    {
        "id": 580,
        "description": "<p>Standing triceps pushdown vertical (not extension) with a V-shaped bar</p>",
        "name": "Triceps Pushdown V-shape Bar",
        "name_original": "Triceps Pushdown V-shape bar",
        "uuid": "38ba2058-96d9-4926-8089-612ba3cc74a7",
    
    },
    {
        "id": 318,
        "description": "<p>Starting on back, move to the standing position with dumbbell in one hand.  Switch hands between reps.</p>",
        "name": "Turkish Get-Up",
        "name_original": "Turkish Get-Up",
        "uuid": "bd07fc6b-db86-4139-b6de-e328cea0f694",
    },
    
    {
        "id": 212,
        "description": "<p>Grip the pull-down bar with your palms facing you and your hands closer than shoulder-width apart. Lean back slightly and keep your back straight. Pull the bar down towards your chest, pulling your shoulders back slightly at the end of the motion.</p>",
        "name": "Underhand Lat Pull Down",
        "name_original": "Underhand lat pull down",
        "uuid": "bf618c32-b4b1-4bbb-a74f-73485e4143ed",
    
    },
    
    {
        "id": 554,
        "description": "<ol>\n<li>Select the desired weight and load it onto a barbell.</li>\n<li>Unrack the bar by rotating the safety latches off the j-hooks.</li>\n<li>Inhale, brace your abs, and then lead the movement by driving the elbows high as you pull the bar to chest height.</li>\n<li>When the bar has reached its peak, reverse the movement slowly while controlling the bar back to the starting position.</li>\n<li>Repeat for the desired number of repetitions.</li>\n</ol>",
        "name": "Upright Row, Smith Machine",
        "name_original": "Upright Row, Smith Machine",
        "uuid": "b0bd1f4c-f4f2-4ba4-b66b-bccb0d743d6e",
    },
    {
        "id": 127,
        "description": "<p>Stand straight, your feet are shoulder-width apart. Hold the SZ-bar with an overhand grip on your thighs, the arms are stretched. Lift the bar close to the body till your chin. The elbows point out so that at the highest point they form a V. Make here a short pause before going slowly down and repeating the movement.</p>",
        "name": "Upright Row, SZ-bar",
        "name_original": "Upright row, SZ-bar",
        "uuid": "61271a8e-6a00-4898-8dc3-ea9ef913f25e",
    },

    {
        "id": 518,
        "description": "<p>throw a medicine ball against a 3m high target and squat down.</p>\n<p>9kg medicine ball for man</p>\n<p>6kg for women </p>",
        "name": "Wallballs",
        "name_original": "wallballs",
        "uuid": "7de7dffb-c814-4228-b41b-ffd133093637",
    },
    
    {
        "id": 548,
        "description": "<p>Stand with heels,  shoulders, back of head,  and hips touching the wall. Start with biceps straight out and elbows at a 90 degree angle. Straighten the arms while remaining againstthe wall without arching the back off of the wall, mimicking a shoulder press movement. </p>",
        "name": "Wall Slides",
        "name_original": "Wall slides",
        "uuid": "e934e2b2-abe7-411d-b753-8ce298391b9e",
       },
    {
        "id": 387,
        "description": "<p>Find a nice flat piece of wall and stand with your back leaning against the wall. Slowly slide down the wall while moving your feet away from it, until your thighs are parallel to the ground and both your knees and your hips are bent at a 90° angle. Cross your arms in front of your chest and hold this position for 30 seconds.</p>\n<p>Variant: put a big inflated rubber ball (like a small basketball) between your knees and squeeze the ball while holding the squat position</p>",
        "name": "Wall Squat",
        "name_original": "Wall Squat",
        "uuid": "b229c57f-5363-41e2-add7-c2501a31de0b",

    },
    
    {
        "id": 485,
        "description": "<p>Forearm wrist Curls. </p>\n<p> </p>\n<ol>\n<li>Sit on a flat bench with a dumbbell in your right hand.</li>\n<li>Place your feet flat on the floor, at a distance that is slightly wider than shoulder width apart.</li>\n<li>Lean forward and place your right forearm on top of your upper right thigh with your palm up. <strong>Tip:</strong> Make sure that the front of the wrist lies on top of your knees. This will be your starting position.</li>\n<li>Lower the dumbbell as far as possible as you keep a tight grip on the dumbbell. Inhale as you perform this movement.</li>\n<li>Now curl the dumbbell as high as possible as you contract the forearms</li>\n<li>Perform for the recommended amount of repetitions, switch arms and repeat the movement.</li>\n</ol>\n<p> </p>",
        "name": "Wrist Curl",
        "name_original": "Wrist Curl",
        "uuid": "963d302f-9c1a-49d7-91a1-47a2e755c0a4",
    },
    {
        "id": 288,
        "description": "<p>Grab barbell and place on shoulders then walk</p>",
        "name": "Yolk Walks",
        "name_original": "yolk walks",
        "uuid": "8d5d8cc1-3ea5-4ca1-bb43-4bfaef9e7098",
    },
    {
        "id": 305,
        "description": "<p>-Perform a traditional dumbbell biceps curl, pausing at the top of the motion.</p>\n<p>-Twist your hands until your palms are facing away from your shoulders (in reverse curl position).  Basically, twist your right wrist inwards to the left, and vice versa.</p>\n<p>-In a slow, controlled movement, lower the Dbells with your palms facing the ground.  At the bottom of the motion, twist your wrists back into the traditional curl grip (palms facing up, towards shoulder).</p>\n<p>-That is 1 rep</p>\n<p> </p>",
        "name": "Z Curls",
        "name_original": "Z Curls",
        "uuid": "7738fb4e-8f17-4aeb-84ef-2ee3e12eaff7",
    }
]

const getExerciseId = (exerciseName) => {
    let exerciseId = [];
    exercises.forEach((elem) => {
        if (elem.name.includes(exerciseName)) {
            exerciseId.push(elem.id);
        }
    });
    return exerciseId;
}

export default getExerciseId;