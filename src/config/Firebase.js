import app from 'firebase/app'
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyD7JYJhYiX98eIsrJjcVqiamGnQ24VKHWE",
    authDomain: "noobfit-react.firebaseapp.com",
    databaseURL: "https://noobfit-react.firebaseio.com",
    projectId: "noobfit-react",
    storageBucket: "noobfit-react.appspot.com",
    messagingSenderId: "658072892659",
    appId: "1:658072892659:web:5c0c031f99d22fb5af3386"
  };

class FirebaseApp {
    constructor(){
        app.initializeApp(firebaseConfig);
        this.auth = app.auth();
    }

    doCreateUserWithEmailAndPassword = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) => this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();
}

export default FirebaseApp;

