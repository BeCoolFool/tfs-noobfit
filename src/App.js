import React,{Fragment} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import { withAuthentication } from './components/SessionHOC/index'


import Footer from './components/Footer/Footer'
import Nav from './components/Nav/Nav'
import CreateTrain from './components/CreateTrain/CreateTrain'
import Login from './components/Login/Login'
import Registration from './components/Registration/Registration'
import SearchEx from './components/SearchEx/SearchEx'
import GreetingPage from './components/GreetingPage/GreetingPage'
import Home from './components/Home/Home'

import './App.css'


const App = () => {
    return (
      <Fragment>
        <Router>
          <Nav/>
          <div className="content">
            <Switch>
              <Route path='/' exact component={GreetingPage}/>
              <Route path='/create_train' exact component={CreateTrain}/>
              <Route path='/search_ex' exact component={SearchEx}/>
              <Route path='/login' exact component={Login}/>
              <Route path='/registration' exact component={Registration}/>
              <Route path ='/home' exact component={Home}/>
              <Route path='/something' render={() => <h2>Something went wrong...</h2>}/>
            </Switch>
          </div>
        </Router>
        <Footer />
      </Fragment>
  )
}


export default withAuthentication(App);


