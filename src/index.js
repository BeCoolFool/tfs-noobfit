import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import FirebaseApp from './config/Firebase';
import FirebaseContext from './config/context'

ReactDOM.render(
  <FirebaseContext.Provider value={new FirebaseApp()}>
    <App />
  </FirebaseContext.Provider>,
  document.getElementById('root'),
);
