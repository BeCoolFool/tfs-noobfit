import React from 'react';
import { withFirebase } from '../../config/index';

import './Nav.css'



const SignOutButton = ({ firebase }) => {
  const outAndClean = () => {
    firebase.doSignOut();
    localStorage.clear();
  }

  return (
  <button type="button" onClick={outAndClean} className='signout-btn'>
    Sign Out
  </button>
  )
}
export default withFirebase(SignOutButton);