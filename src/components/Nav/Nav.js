import React from 'react'
import {Link} from 'react-router-dom'
import SignOutButton from './SignOutButton'
import {AuthUserContext} from '../SessionHOC/index'

import './Nav.css'


const Nav = ({authUser}) => {
    return <AuthUserContext.Consumer>
                {authUser => authUser ? <NavAuth /> : <NavNonAuth />}
            </AuthUserContext.Consumer>
    }

const NavAuth = () => {
    return (
        <nav className="site-header sticky-top py-1">
            <div className="container d-flex flex-column flex-md-row justify-content-between">
                <span className="py-2 d-none d-md-inline-block active" href="#"><Link to='/' className='link-style'>Main page</Link></span>
                <span className="py-2 d-none d-md-inline-block" href="#"><Link to='/search_ex' className='link-style'>Exercise search</Link></span>
                <span className="py-2 d-none d-md-inline-block" href="#"><Link to='/home' className='link-style'>Private account</Link></span>
                <span className="py-2 d-none d-md-inline-block active" href="#"><Link to='/create_train' className='link-style'>Create training</Link></span>
                <span className="py-2 d-none d-md-inline-block active" href="#"><SignOutButton/></span>
            </div>
        </nav>
    );
}

const NavNonAuth = () => {
    return (
        <nav className="site-header sticky-top py-1">
            <div className="container d-flex flex-column flex-md-row justify-content-between">
                <span className="py-2 d-none d-md-inline-block active" href="#"><Link to='/' className='link-style'>Main page</Link></span>
                <span className="py-2 d-none d-md-inline-block" href="#"><Link to='/search_ex' className='link-style'>Exercise search</Link></span>
                <span className="py-2 d-none d-md-inline-block" href="#"><Link to='/login' className='link-style'>Private account</Link></span>
            </div>
        </nav>
    );
}

export default Nav;
