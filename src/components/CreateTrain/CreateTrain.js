import React, { useState, useMemo, Fragment } from "react";
import './CreateTrain.css'
import TrainingTemplate from '../TrainingTemplate/TrainingTemplate'
import { withAuthorization } from '../SessionHOC/index'


//selected={option === selected} na 11 stroke
const Select = ({ options, selected, defaultVar, onChange, optionRender }) => {
  return (
    <select onChange={e => onChange(options[e.target.value])} defaultValue={defaultVar}>
        <option value={defaultVar} disabled>{defaultVar}</option>
            {options.map((option, index) => (
                <option value={index} key={index}>
                    {optionRender(option, index)}
                </option>
            ))
        }
    </select>
  );
};




const trainings = [
  { type: "Gain mass" },
  { type: "Lose weight" }
];

const daysGain = [
    { num: "2" }, 
    { num: "3" }, 
    { num: "4" }
];

const daysLoose = [ 
    { num: "3" }, 
    { num: "4" }
];




function CreateTrain() {
  const [selectedTraining, setSelectedTraining] = useState(null);
  const [selectedDays, setSelectedDays] = useState(null);


  const resultingComponent = useMemo(() => {
    if (!selectedTraining || !selectedDays) {
        return null;
    }
    
    return <TrainingTemplate training={selectedTraining} day={selectedDays} />;
  }, [selectedTraining, selectedDays]);

  return (
    <div className="container train-container">
      <h1 className='train-header'>Create your own workout!</h1>
      <div className='train-params'>
        <span>Choose type of training: </span>
        <Select
          options={trainings}
          selected={selectedTraining}
          optionRender={trainings => trainings.type}
          onChange={setSelectedTraining}
          defaultVar='Choose type of training'
        />
        {selectedTraining && (
          <Fragment>
            <span>Choose number of training days: </span>
            <Select
              options={selectedTraining.type === "Gain mass" ? daysGain : daysLoose}
              selected={selectedDays}
              optionRender={days => days.num}
              onChange={setSelectedDays}
              defaultVar='Number of training per week'
            />
          </Fragment>
        )}
      </div>
      {resultingComponent}
    </div>
  );
}

const condition = authUser => authUser != null;

export default withAuthorization(condition)(CreateTrain);