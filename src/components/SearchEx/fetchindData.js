const fetchingData = (exerciseName, url = "https://wger.de/api/v2/exercise/") => {
  return new Promise(resolve => {
    fetch(url)
      .then(data => data.json())
      .then(data => {
        const results = [];
        const next = data.next;
        data.results.forEach((elem) => {
          if (elem.name.includes(exerciseName)) {
            results.push(elem.id);
          }
        });
        if (next) {
          fetchingData(exerciseName, next)
            .then(lastResults => {
              results.push(...lastResults);
              resolve(results);
            });
        } else {
          resolve(results);
        }
      });
  });
};

export default fetchingData;
