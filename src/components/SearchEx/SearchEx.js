import React, {Component, Fragment} from 'react'
import ExerciseTemplate from '../ExerciseTemplate/ExersciseTemplate'
import getExerciseId from '../../utils/exerciseId'

import './SearchEx.css'

export default  class SearchEx extends Component {
    state = {
        label: '',
        exercise: null
    }

    inputChange = (event) => {
        this.setState({
            label: event.target.value
        })
    }

    formSubmit = (event) => {
        event.preventDefault();
        const data = getExerciseId(this.state.label);
        this.setState({
                label: '',
                exercise: data
            })
    }
    render() {
        const {exercise} = this.state;
        let exerciseList;
        if(exercise){
            if(exercise.length === 0){
                exerciseList = <h2 className='no-exercise'>Such exercise doesn`t exist</h2>
            } else {
                exerciseList = exercise.map(elem => <ExerciseTemplate info={elem} key={elem}/>);
            }
        }
        return (
            <Fragment>
                <form className="form-search" onSubmit={this.formSubmit}>
                    <div className="form-input">
                        <label htmlFor="exersiceSearch" className="sr-only">Password</label>
                        <input type="text" id="exersiceSearch" className='input' onChange={this.inputChange} placeholder="Enter exercise name"/>
                    </div>
                    <button type="submit" className="btn btn-primary mb-2">Search</button>
                </form>
                <div>
                    {exerciseList}
                </div>
            </Fragment>
        )
    }
}