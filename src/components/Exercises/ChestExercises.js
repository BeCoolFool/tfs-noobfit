import React from 'react'
import './exercises.css'

const ChestExercises = () => {
    const atlas = {
        begining: ['Barbell Bench Press', 'Low-Incline Barbell Bench Press', 'Seated Machine Chest Press'],
        mid: ['Flat Bench Dumbbell Press', 'Incline Dumbbell Press', 'Incline Bench Cable Fly'],
        ending: ['Incline Dumbbell Pull-Over', 'Butterfly' , 'Push ups']
    }

    return (
        <div className='ex-container'>
            <h3>Chest</h3>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Chest_1', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.begining.map((elem,id) => <option value={elem} key={id*2}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Chest_2', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.mid.map((elem,id) => <option value={elem} key={id*3}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Chest_3', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.ending.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
        </div>
    )
}

export default ChestExercises;