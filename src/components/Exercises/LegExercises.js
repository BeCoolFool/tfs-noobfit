import React from 'react'
import './exercises.css'

const LegExercises = () => {
    const atlas = {
        begining: ['Romanian Deadlift', 'Leg Press ', 'Sumo Squat', 'Squats on Smith'],
        mid: ['Box jump', 'Lateral Lunge', 'Step Up', 'Squats with ball'],
        ending: ['Leg Extension', 'Reverse Leg Extension', 'Seated calf raises', 'Hamstring curls']
    }

    return (
        <div className='ex-container'>
            <h3>Legs</h3>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Legs_1', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.begining.map((elem,id) => <option value={elem} key={id*2}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Legs_2', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.mid.map((elem,id) => <option value={elem} key={id*3}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Legs_3', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.ending.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
        </div>
    )
}

export default LegExercises;