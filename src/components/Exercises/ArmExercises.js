import React from 'react'
import './exercises.css'

const ArmExercises = () => {
    const atlas = {
        begining: ['Military bench press', 'Arnold press', 'Side lateral raises', 'Seated Bent-Over Rear Delt Raise'],
        mid: ['Hammer curl', 'Dumbbell curl', 'Wide-Grip Standing Barbell Curl', 'EZ-Bar Curl'],
        ending: ['Skullcrusher','Seated Overhead Dumbbell Extension', 'Cable Push-Down', ' Parallel-Bar Dip']
    }
    return (
        <div className='ex-container'>
            <h3>Arms</h3>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Arm_1', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.begining.map((elem,id) => <option value={elem} key={id*2}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Arm_2', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.mid.map((elem,id) => <option value={elem} key={id*3}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Arm_3', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.ending.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
        </div>
    )
}

export default ArmExercises;