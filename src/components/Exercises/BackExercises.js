import React from 'react'
import './exercises.css'

const BackExercises = () => {
    const atlas = {
        begining: ['Pullup or Chinup Variations','Deadlift', 'Straight leg deadlift', 'Bent-Over Barbell Rows'],
        mid: ['Lat Pulldown','Pullover','Bent-Over Barbell Rows', 'Dumbbell Single Arm Row'],
        ending: ['T-bar row','Horizontal pullups','Romanian deadlift with dumbbells','Hyperextension']
    }

    return (
        <div className='ex-container'>
            <h3>Back</h3>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Back_1', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.begining.map((elem,id) => <option value={elem} key={id*2}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Back_2', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.mid.map((elem,id) => <option value={elem} key={id*3}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem('E!@#$%^_Back_3', event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.ending.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
        </div>
    )
}

export default BackExercises;
