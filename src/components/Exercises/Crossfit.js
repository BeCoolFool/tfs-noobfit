import React from 'react'
import './exercises.css'

const CrossFit = ({workNum}) => {
    const atlas = {
        begining: ['Burpee', 'Box Jump', 'Ketlleball swing with arm change', 'Deadlift'],
        mid: ['Squats with ball', 'Plate squate press', 'Box step with dumbbell', 'Split squat jumps'],
        ending: ['Mountain climbers', 'Skipping','Ketlleball swing', 'Plie squat'],
        other: ['Pull ups', 'Push ups', 'Dumbell Raise', 'EZ-bar curl'],
        bonus: ['None', 'Full crunches with ball', 'Crunches', 'Plank', 'Bicycle crunches']
    }

    return (
        <div className='ex-container'>
            <h3>Fullbody training day</h3>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem(`E!@#$%^_Fullbody_${workNum}_1`, event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.begining.map((elem,id) => <option value={elem} key={id*2}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem(`E!@#$%^_Fullbody_${workNum}_2`, event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.mid.map((elem,id) => <option value={elem} key={id*3}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem(`E!@#$%^_Fullbody_${workNum}_3`, event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.ending.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem(`E!@#$%^_Fullbody_${workNum}_4`, event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.other.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
            <select className='select-bar' defaultValue='Choose exercise' onChange={event => {
                localStorage.setItem(`E!@#$%^_Fullbody_${workNum}_5`, event.target.options[event.target.selectedIndex].text);
            }}>
                <option value='Choose exercise' disabled>Choose exercise</option>
                {atlas.bonus.map((elem,id) => <option value={elem} key={id*4}>{elem}</option>)}
            </select>
        </div>
    )
}

export default CrossFit;