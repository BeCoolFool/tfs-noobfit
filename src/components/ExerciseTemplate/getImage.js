const getImage = (exerciseId, url = "https://wger.de/api/v2/exercise/") => {
  return new Promise(resolve => {
    fetch(url)
      .then(data => data.json())
      .then(data => {
        const results = [];
        const next = data.next;
        data.results.forEach((elem) => {
          if (elem.exercise === exerciseId) {
            results.push(elem.image);
          }
        });
        if (next) {
          getImage(exerciseId, next)
            .then(lastResults => {
              results.push(...lastResults);
              resolve(results);
            });
        } else {
          resolve(results);
        }
      });
  });
};

export default getImage;