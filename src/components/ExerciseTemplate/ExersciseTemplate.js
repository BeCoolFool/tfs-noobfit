import React, {Component} from 'react'
import getImage from '../../utils/images'
import './ExersciseTemplate.css'

export default class ExersiceTemplate extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: null,
            category: null,
            description: null,
            muscles: null,
            muscles_secondary: null,
            equipment: null,
            image: null,
        }
    }

    keyId = 100;

    componentDidMount = () => {

        fetch(`https://wger.de/api/v2/exerciseinfo/${this.props.info}/?format=json`)
        .then(data => data.json())
        .then(data => {
            this.setState({
                name: data.name,
                category: data.category,
                description: data.description,
                muscles: data.muscles,
                muscles_secondary: data.muscles_secondary,
                equipment: data.equipment,
                image: getImage(this.props.info)
            })
        })
    }

    checkData = (element) => {
        if(Array.isArray(element)){
            return element.length === 0 ? 'None' : element.map(el => el.name).join(', ');
        }
    }

    clearString = (text) => {
        return typeof text === 'string' ? text.replace(/<[^>]+>/g, '') : null;
    }

    render() {
        const {name, description, muscles, muscles_secondary, equipment, category, image} = this.state;

        return (
            <div className="exercise-info">
                <h3>{name}</h3>
                {category === null ? null : <span><b>Category:</b> {category.name}</span>}
                <p><b>Description: </b>{this.clearString(description)}</p>
                {image === null ? null : 
                        <div className="image-block">
                            {image.map(elem => <img src={elem} key={this.keyId++} alt='exercise' className='image'/>)}
                        </div>
                }
                <span><b>Muscules:</b> {this.checkData(muscles)}</span>
                <span><b>Secondary muscules:</b> {this.checkData(muscles_secondary)}</span>
                <span><b>Equipment:</b> {this.checkData(equipment)}</span>
            </div>
        )
    }
}