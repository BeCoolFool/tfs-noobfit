import React from 'react'

import './Footer.css'

const Footer = () => {
    return (
        <footer className='footer mt-auto py-3 footer-fixed-bottom bg-secondary'>
            <div className='container text-center'>
                <span className='text-white'>Tinkoff Fintech, 2019</span>
            </div>
        </footer>
    );
}

export default Footer;