import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {FirebaseContext, withFirebase } from '../../config/index';
import {compose} from 'recompose'


const Registration = () => (
  <div>
    <h1 className="mt-4 text-center">Sign Up</h1>
    <FirebaseContext.Consumer>
      {firebase => <SignUpForm firebase={firebase} />}
    </FirebaseContext.Consumer>
  </div>
);


class SignUpFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
        email: '',
        password: '',
        username: '',
        error: null
    };
  }

  onSubmit = event => {
    const { email, password } = this.state;

    this.props.firebase
      .doCreateUserWithEmailAndPassword(email, password)
      .then(authUser => {
        this.setState({ 
            email: '',
            password: '',
            username: '',
            error: null
        });
        this.props.history.push('/home');
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const {
      username,
      email,
      password,
      error,
    } = this.state;



    return (
      <form onSubmit={this.onSubmit} className='form_signin'>
        <input
          name="username"
          value={username}
          onChange={this.onChange}
          type="text"
          placeholder="Full Name"
          className='form-control'
        />
        <input
          name="email"
          value={email}
          onChange={this.onChange}
          type="text"
          placeholder="Email Address"
          className='form-control'
        />
        <input
          name="password"
          value={password}
          onChange={this.onChange}
          type="password"
          placeholder="Password"
          className='form-control'
        />
        <button type="submit" className='btn btn-lg btn-primary btn-block'>
          Sign Up
        </button>

        {error && <p>{error.message}</p>}
      </form>
    );
  }
}

const SignUpForm = compose(
    withRouter,
    withFirebase,
  )(SignUpFormBase);;

export default Registration;
