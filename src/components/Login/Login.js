import React, {Component} from 'react'
import './Login.css'


import {Link,  withRouter } from 'react-router-dom';
import { withFirebase } from '../../config/index';
import {compose} from 'recompose'



const Login = () => <SignInForm />



class SignInFormBase extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            error: null
        }
    }

    onChange = (event) =>{
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    handleSubmit = (event) => {
        const {email, password} = this.state;

        this.props.firebase
        .doSignInWithEmailAndPassword(email, password)
        .then(() => {
            this.setState({ 
                email: '',
                password: '',
                error: null
            });
            this.props.history.push('/home');
        })
        .catch(error => {
            this.setState({ error });
        });

      event.preventDefault();
    };
    
    
    render() {
        const { email, password, error } = this.state;

        return (
            <form className='form_signin' onSubmit={this.handleSubmit}>
                <h1 className="h3 mb-3 font-weight-normal">Login</h1>
                <input type='text'
                    name='email'
                    value={email}
                    className='form-control'
                    placeholder='E-mail' 
                    onChange={this.onChange}
                />
                <input type='password'
                    name='password'
                    className='form-control'
                    placeholder='Password' 
                    value={password}
                    onChange={this.onChange}
                />
                <button className='btn btn-lg btn-primary btn-block'>Sign in</button>
                {error && <p>{error.message}</p>}
                <p>Don't have an account? <Link to='/registration'>Sign Up</Link></p>
            </form>
        );
    }
}

const SignInForm = compose(
    withRouter,
    withFirebase,
  )(SignInFormBase);

  
export default Login;