import React from 'react'

import logo1 from './logo1.png'
import logo2 from './logo2.png'
import logo3 from './logo3.png'

import './GreetingPage.css'

const GreetingPage = () => {
    return (
        <div className='container greet-cont'>
            <h2>Welcome to Noobfit!</h2>
            <p>The NoobFit application allows you to select ready-made workouts, draw up a training plan depending on the purpose of the workouts 
                and on the number of possible training days. Main functions:</p>
                <ul className="list-group">
                    <li className="list-group-item li-item">Selection of a training program depending on the number of training days and the purpose of training</li>
                    <li className="list-group-item li-item">Searcher that can find exercise by it`s name and give you it`s description.</li>
                    <li className="list-group-item li-item">Personal account that stores your workouts.</li>
                </ul>
            <div className='img-greet-cont'>
                <img src={logo1} alt='logo' width='100' height='100'/>
                <img src={logo2} alt='logo' width='200' height='200'/>
                <img src={logo3} alt='logo' width='100' height='100'/>
            </div>
        </div>
    )
}

export default GreetingPage;

