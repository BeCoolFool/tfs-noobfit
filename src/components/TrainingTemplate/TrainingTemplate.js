import React, {Component,Fragment} from 'react'

import './TrainingTemplate.css'
import trainCompil from './trainCompil'

export default class TrainingTemplate extends Component{
    constructor(props){
        super(props);
        this.state = {
            training: this.props.training.type,
            day: this.props.day.num,
            trainingDays: [],
            firsttime: true,
            selectedWorkout: []
        };
    }


    static getDerivedStateFromProps = (props, state) => {

        if(state.firsttime || (props.day.num !== state.day)){
            localStorage.clear();
            return trainCompil(props, false);
        }
        else if(props.training !== state.training){
            localStorage.clear();
            return trainCompil(props, true);
        } else {
            return null;
        }
    }

    render() {
        const {trainingDays} = this.state;
        const GainTip = <span className='ex-tip'>Perform 3 sets of 15 reps with a rest of 1 minute. Between sets, rest for 2 - 3 minutes. Dont use heavy weights.</span>
        const LooseTip = <span className='ex-tip'>Perform 3 sets of 20 reps with a rest of 1 - 1.5 minutes. Between sets, rest for 2 - 3 minutes. If you start feeling dizzy, stop your training!</span>
        return (
            <Fragment>
                <div className='workout-part'>
                    {trainingDays}
                </div>
                <p className='ex-tip-p'>{this.props.training.type === "Gain mass" ? GainTip : LooseTip}</p>
            </Fragment>
        );
    }
}