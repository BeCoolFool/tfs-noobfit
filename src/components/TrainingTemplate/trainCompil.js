import React from 'react'
import BackExercises from '../Exercises/BackExercises'
import ArmExercises from '../Exercises/ArmExercises'
import ChestExercises from '../Exercises/ChestExercises'
import LegExercises from '../Exercises/LegExercises'
import CrossFit from '../Exercises/Crossfit'

const trainCompil = (props, change, handleChange) => {
    const daysNum = change ? 3 : props.day.num;
    localStorage.setItem('day', daysNum);
    const elements = (props.training.type === "Gain mass") ? [<BackExercises handleChange={handleChange}/>, <ChestExercises handleChange={handleChange}/>, <LegExercises handleChange={handleChange}/>, <ArmExercises handleChange={handleChange}/>, null] : 
            [<CrossFit workNum={'1'}/>, <CrossFit workNum={'2'}/>, <CrossFit workNum={'3'}/>, <CrossFit workNum={'4'}/>, null];
            const temp = [];
            const groupPerDay =  parseInt(elements.length/daysNum);
            let keyId = 200;
            
            for(let i = 0, j = groupPerDay, k = 0; i < daysNum; i++){
                temp.push(
                    <div key={keyId++} className='training-day'>
                        {elements.slice(k, j)}
                    </div>
                );
                j += groupPerDay;
                k += groupPerDay;
            }

            return {
                trainingDays: [...temp],
                firsttime: false,
                day: daysNum
            }
}

export default trainCompil;