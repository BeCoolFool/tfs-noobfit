import React, {Fragment, Component} from 'react'
import { withAuthorization } from '../SessionHOC/index'
import './Home.css'

const showWorkout = () => {
    let keys = Object.keys(localStorage).filter(elem => elem.includes('E!@#$%^_'));
    if(!keys.length){
        return 'You don`t have any workouts';
    } else {
        let temp = [];
        keys.sort().forEach(elem => {
            temp.push(localStorage.getItem(elem));
        });
        const workout = temp.map(elem => <li className='list-group-item'>{elem}</li>);
        return workout;
    }
}

class Home extends Component {
    state = {
        memory: null
    }

    componentDidMount = () => {
        this.setState({
            memory: showWorkout(),
        })
    }

    render() {
    return (
        <Fragment>
        <div className='container'>
            <h2 className='text-center'>My exercises</h2>
            <ul className='list-group'>
                {this.state.memory ? this.state.memory : null}
            </ul>
        </div>
        </Fragment>
    )
    }
}

const condition = authUser => authUser != null;

export default withAuthorization(condition)(Home);